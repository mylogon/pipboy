//
//  Screen.m
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "Screen.h"
#import "ScreenData.h"
#import "PBTableViewCell.h"
#import "PBTableView.h"
#import "ScreenSetup.h"
#import "UIImage+(UIImageColorReplacement).h"

@interface UITextView (category)
-(void)resizeFont;
@end
@implementation UITextView (category)

-(void)resizeFont{
    
    [self setFont:[UIFont fontWithName:@"Monofonto-Regular" size:12]];
    
    while (((CGSize) [self sizeThatFits:self.frame.size]).height > self.frame.size.height) {
        self.font = [self.font fontWithSize:self.font.pointSize-1];
    }
}
@end

@implementation Screen

-(instancetype)init{
    self=[super initWithFrame:[UIScreen mainScreen].bounds];
    return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.objects.rowObjects.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return [self getCellForTable:tableView AtIndexPath:indexPath].rowObject.cellHeight;
}

-(PBTableViewCell*)getCellForTable:(UITableView*)tableView AtIndexPath:(NSIndexPath*)path{
    PBTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[PBTableViewCell identifier]];
    
    if (!cell) {
        cell = [[PBTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault
                                     reuseIdentifier:[PBTableViewCell identifier]];
    }
    
    RowObject * object = _data.objects.rowObjects[path.row];
    [cell loadData:object
       inCellWidth:_table.frame.size.width];
    
    return cell;
}

-(PBTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [self getCellForTable:tableView AtIndexPath:indexPath];
}

-(void)initialSelect{
    NSIndexPath * initialPath = [NSIndexPath indexPathForRow:0 inSection:0];
    PBTableViewCell * initialCell = [_table cellForRowAtIndexPath:initialPath];
    [_table selectRowAtIndexPath:initialPath
                        animated:NO
                  scrollPosition:UITableViewScrollPositionNone];
    [self selectCell:initialCell atIndexPath:initialPath];
    
}

-(void)tappedTop{
    
    NSIndexPath * currentPath = [_table indexPathForSelectedRow];
    
    for (int x = 0; x < currentPath.row; x++) {
        [self performSelector:@selector(swipeUp)
                   withObject:nil
                   afterDelay:x * 0.12]; //for now, magic number.
    }
}

-(void)swipeUp{
    if (_table && [_table indexPathForSelectedRow].row > 0 ) {
        NSIndexPath * oldPath = [_table indexPathForSelectedRow];
        NSIndexPath * newPath = [NSIndexPath indexPathForRow:[_table indexPathForSelectedRow].row - 1 inSection:0];
        
        [self cellSelection:oldPath new:newPath];
    }
}

-(void)swipeDown{
    
    if (_table && [_table indexPathForSelectedRow].row < _data.objects.rowObjects.count - 1) {
        
        NSIndexPath * oldPath = [_table indexPathForSelectedRow];
        NSIndexPath * newPath = [NSIndexPath indexPathForRow:[_table indexPathForSelectedRow].row + 1 inSection:0];
        
        [self cellSelection:oldPath new:newPath];
    }
}

-(void)cellSelection:(NSIndexPath*)old new:(NSIndexPath*)new{
    
    PBTableViewCell * current = [_table cellForRowAtIndexPath:old];
    [self deselectCell:current atIndexPath:old];
    
    PBTableViewCell * initialCell = [_table cellForRowAtIndexPath:new];
    [self selectCell:initialCell atIndexPath:new];
    
    //not in selectCell:atIndex so initialSelect doesn't play it
    [SoundEffects playTableScroll];
    
}

-(void)selectCell:(PBTableViewCell*)cell atIndexPath:(NSIndexPath*)path{
    
    RowObject *obj = _data.objects.rowObjects[path.row];
    
    [_table selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    [cell select];
    [self tableView:_table didSelectRowAtIndexPath:path];
    
    [_infoTextView setText:obj.info];
    [_infoTextView resizeFont];
    
    if (obj.imageName) {
        [_infoImageView setImage:[[UIImage imageNamed:obj.imageName] swapOutWhiteForGreen]];
    }
    
    if (_weightLabel) {
        if (obj.weight > 0) {
            [_weightLabel setText:[NSString stringWithFormat:@"%d",obj.weight]];
        }else{
            [_weightLabel setText:@"--"];
        }
    }
    
    if (_valueLabel) {
        [_valueLabel setText:[NSString stringWithFormat:@"%d",obj.value]];
    }
    
    if (_effectsLabel) {
        [_effectsLabel setText:obj.effects];
    }
    
    if (_ammoTypeTextView) {
        [_ammoTypeTextView setText:[AmmoTypes getTitle:obj.takesAmmo]];
    }
    
    if (_damageLabel) {
        [_damageLabel setText:[NSString stringWithFormat:@"%d",obj.damage]];
    }
    
    if (_conditionView) {
        [_conditionView setCondition:obj.condition];
    }
    
    if (_damageResLabel) {
        [_damageResLabel setText:[NSString stringWithFormat:@"%d",obj.damage]];
    }
}

-(void)deselectCell:(PBTableViewCell*)cell atIndexPath:(NSIndexPath*)path{
    [_table deselectRowAtIndexPath:path animated:NO];
    [cell deselect];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    //Puts that little box on the left
//    PBTableViewCell * tableCell = [tableView cellForRowAtIndexPath:indexPath];
//    [tableCell.rowObject.belongsToScreen.data setAllToDeselected];
//    [tableCell.rowObject setIsSelected:YES];
//    [tableView reloadData];

    //TODO:: This will probably need to move to a tap gesture method ^
    
    float scrollPercent = (float)indexPath.row / (_data.objects.rowObjects.count -1);
    [_scrollBar tableScrolled:scrollPercent];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = _table.bounds;
    
    float currentOffset = tableView.contentOffset.y;
    float totalSize = tableView.contentSize.height;
    float currentBottom = tableView.contentOffset.y + tableView.bounds.size.height;
    
    if (currentOffset > 0 && (int)currentBottom == (int)totalSize) {
        //scrolled to bottom
        gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor,
                                 (id)[UIColor clearColor].CGColor];
        gradientLayer.startPoint = CGPointMake(0.0f, 0.3f);
        gradientLayer.endPoint = CGPointMake(0.0f, 0.0f);
        _table.layer.mask = gradientLayer;
    }else if (currentOffset == 0 && totalSize > currentBottom){
        //scrolled to top but there is more below
        
        gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor,
                                 (id)[UIColor clearColor].CGColor
                                 ];
        gradientLayer.startPoint = CGPointMake(0.0f, 0.7f);
        gradientLayer.endPoint = CGPointMake(0.0f, 1.0f);
        _table.layer.mask = gradientLayer;
    }else if (currentOffset > 0 && currentBottom < totalSize){
        //scrolled down but more to go
        gradientLayer.colors = [NSArray arrayWithObjects:
                                (id)[UIColor clearColor].CGColor,
                                (id)[UIColor whiteColor].CGColor,
                                (id)[UIColor whiteColor].CGColor,
                                (id)[UIColor clearColor].CGColor,nil];
        
        gradientLayer.locations = @[@0.0,@0.3,@0.7,@1.0];
        _table.layer.mask = gradientLayer;
    }
}

@end
