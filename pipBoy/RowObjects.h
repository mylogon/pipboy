//
//  RowObjects.h
//  pipBoy
//
//  Created by Luke Sadler on 21/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RowObject.h"

@class ScreenData;
@interface RowObjects : NSObject

-(instancetype)initFromScreenData:(ScreenData*)screen;

- (void)newObject:(void(^)(RowObject *))object;

-(void)sortObjectsAlpha;

-(void)randomSetCondition;

@property (nonatomic, strong) NSMutableArray <RowObject *> * rowObjects;
@property (readonly) ScreenData * screenData;

@end
