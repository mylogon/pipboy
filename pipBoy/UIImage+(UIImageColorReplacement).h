//
//  UIColor (UIImageColorReplacement).h
//  pipBoy
//
//  Created by Luke Sadler on 27/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageColorReplacement)

-(UIImage*)swapOutWhiteForGreen;

@end
