//
//  Screen.h
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RowObjects.h"
#import "PBTableView.h"
#import "ScreenEnums.h"
#import "CustomProgressView.h"
#import "ScrollBar.h"

@class ScreenData;
@interface Screen : UIView <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) ScreenType screenType;
@property (nonatomic) PBTableView * table;
@property (nonatomic) ScrollBar * scrollBar;

@property (nonatomic) UIImageView * infoImageView;
@property (nonatomic) UITextView * infoTextView;
@property (nonatomic) ScreenData * data;

@property (nonatomic) UILabel * weightLabel;
@property (nonatomic) UILabel * valueLabel;

/**Weapons property*/
@property (nonatomic) UILabel * strengthLabel;
/**Weapons property*/
@property (nonatomic) UITextView * ammoTypeTextView;
/**Weapons property*/
@property (nonatomic) UILabel * damageLabel;

/**Apparel property*/
@property (nonatomic) UILabel * damageResLabel;

@property (nonatomic) UILabel * effectsLabel;

@property (nonatomic) CustomProgressView * conditionView;


-(void)initialSelect;
-(void)swipeUp;
-(void)swipeDown;

-(void)tappedTop;

@end
