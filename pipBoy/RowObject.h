//
//  SpecialStatObject.h
//  pipBoy
//
//  Created by Luke Sadler on 21/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AmmoTypes.h"

typedef enum : NSUInteger {
    NoCondition = -1
} ConditionType;

@class Screen;
@interface RowObject : NSObject

//Set when creating a cell so that the height is appropriate for it's content
@property float cellHeight;

/**The text for the cell*/
@property NSString * title;

/**The number for the cell. Level or count*/
@property int level;

/**Text for text box*/
@property NSString * info;

/**Name of the image for the imageview*/
@property NSString * imageName;

/**Lets the cell know which screen they're on. Mock reverse relationship*/
@property Screen * belongsToScreen;

//Sellable items

@property int value;

@property int weight;

/**doubles up as damage resistance for apparel*/
@property int damage;

/**If this is not set, it will be randomised for weapons and apparel*/
@property NSUInteger condition;

@property (nonatomic) TheAmmoType takesAmmo;

//Probably not going use this for now on weapons
@property (nonatomic) NSString * effects;

//Selectable items, such as weapons

@property BOOL isSelected;

//Default to NO
@property BOOL isSelectable;

@end
