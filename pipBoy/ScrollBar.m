//
//  ScrollBar.m
//  pipBoy
//
//  Created by Luke Sadler on 03/01/2017.
//  Copyright © 2017 Luke Sadler. All rights reserved.
//

#import "ScrollBar.h"
#import "ScreenSetup.h"

@interface ScrollBar()
{
    UIView * sBar;
}

@end
@implementation ScrollBar

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        sBar = [[UIView alloc]initWithFrame:CGRectMake(0,
                                                       0,
                                                       frame.size.width,
                                                       frame.size.height / 5)];
        
        [sBar setBackgroundColor:[ScreenSetup greenColour]];
        
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = sBar.bounds;
        
        gradientLayer.colors = [NSArray arrayWithObjects:
                                (id)[UIColor clearColor].CGColor,
                                (id)[UIColor whiteColor].CGColor,
                                (id)[UIColor whiteColor].CGColor,
                                (id)[UIColor clearColor].CGColor,nil];
        
        gradientLayer.locations = @[@0.0,@0.3,@0.7,@1.0];
        sBar.layer.mask = gradientLayer;
        
        [self addSubview:sBar];
    }
    return self;
}

-(void)tableScrolled:(float)progress{
    
    float posibleRange = self.frame.size.height - sBar.frame.size.height;
    CGRect barRect = sBar.frame;
    barRect.origin.y = posibleRange * progress;
    [sBar setFrame:barRect];
}

@end
