//
//  main.m
//  pipBoy
//
//  Created by Luke Sadler on 02/02/2015.
//  Copyright (c) 2015 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
