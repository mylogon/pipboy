//
//  RadVibrate.h
//  pipBoy
//
//  Created by Luke Sadler on 29/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RadVibrate : NSObject

-(void)start;
-(void)stop;

@end
