//
//  SpecialStats.m
//  pipBoy
//
//  Created by Luke Sadler on 21/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "RowObjects.h"
#import "ScreenData.h"

@implementation RowObjects

-(instancetype)initFromScreenData:(ScreenData *)screen{
    if (self=[super init]) {
        _screenData = screen;
        _rowObjects = [NSMutableArray array];
    }
    return self;
}

-(void)newObject:(void (^)(RowObject *))object{
    
    RowObject * obj = [RowObject new];
    object(obj);
    [_rowObjects addObject:obj];
}

-(void)sortObjectsAlpha{
    [_rowObjects sortUsingComparator:^NSComparisonResult(RowObject * obj1, RowObject * obj2){
        return [obj1.title compare:obj2.title];
    }];
}

-(void)randomSetCondition{
    for (RowObject * obj in _rowObjects) {
        if (obj.condition == 0) {
            obj.condition = arc4random()%80 + 20; //condition will be anywhere between 20-100 %
        }
    }
}

@end
