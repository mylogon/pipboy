//
//  PBTableView.m
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "PBTableView.h"

@implementation PBTableView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        [self setSeparatorColor:[UIColor clearColor]];
    }
    return self;
}

@end
