//
//  ScreenData.h
//  pipBoy
//
//  Created by Luke Sadler on 21/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RowObjects.h"
#import "Screen.h"

@interface ScreenData : NSObject

-(void)setAllStatsToLevel:(int)level;
-(void)randomiseStatLevelsBetweenLevel:(int)lower
                             andHigher:(int)higher;
-(void)setAllImageTo:(NSString *)imageName;

/**Lets all the stats/table cell know which screen they're in*/
-(void)setTheScreenType:(Screen*)screen;

-(void)setAllToDeselected;

-(void)setAllToSelectable;

@property NSInteger type;
@property NSString * title;
@property RowObjects * objects;

@property float weight;
@property NSInteger damage;
@property NSInteger quantity;
@property NSInteger value;
@property NSInteger condition;


@end
