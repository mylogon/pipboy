
#import <Foundation/Foundation.h>

@class StaticNoiseController;
@class TorchController;

@protocol BumpMotionDelegate <NSObject>
-(void)bigBump;
@end

@interface BumpMotion : NSObject

@property BOOL pause;
@property (strong) StaticNoiseController <BumpMotionDelegate> * staticControllerDelegate;
@property (strong) TorchController <BumpMotionDelegate> * torchControllerDelegate;

@end
