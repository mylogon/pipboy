//
//  TorchController.h
//  pipBoy
//
//  Created by Luke Sadler on 19/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "StaticNoiseController.h"
#import "BumpMotion.h"

@interface TorchController : NSObject <BumpMotionDelegate>

-(instancetype)initWithView:(UIView*)touchView;

/**Called with a 2 finger, double tap*/
-(void)screenTapped:(UITapGestureRecognizer*)tap;

@property (nonatomic) StaticNoiseController * staticController;

@property (nonatomic,readonly) BOOL isTorchOn;

@end
