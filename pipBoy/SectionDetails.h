//
//  SectionDetails.h
//  pipBoy
//
//  Created by Luke Sadler on 03/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ScreenData;
@interface SectionDetails : NSObject
{
    @protected
    NSArray <ScreenData*> *_datas;
}
+ (instancetype)sharedInstance;

@property  NSArray <ScreenData*>* datas;

@end
