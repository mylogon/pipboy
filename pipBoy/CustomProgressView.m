//
//  CustomProgressView.m
//  pipBoy
//
//  Created by Luke Sadler on 31/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "CustomProgressView.h"
#import "ScreenSetup.h"

@interface CustomProgressView ()
{
    UIView * progressView;
    UILabel * noConditionLabel;
}
@end

@implementation CustomProgressView

-(instancetype)initWithFrame:(CGRect)frame{
    
    frame.origin.y += frame.size.height *.15;
    frame.size.height *= .7;
    frame.size.width = 50;
    
    
    if (self = [super initWithFrame:frame]) {
        
        [self setupSelfBackground];
        
        progressView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, self.frame.size.height)];
        [self addSubview:progressView];
        [progressView setBackgroundColor:[ScreenSetup greenColour]];
        
        noConditionLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [noConditionLabel setTextAlignment:NSTextAlignmentRight];
        [ScreenSetup greenUpTextView:noConditionLabel];
        [noConditionLabel setText:@"--"];
    }
    return self;
}

-(void)setupSelfBackground{
    [self setBackgroundColor:[[ScreenSetup greenColour] colorWithAlphaComponent:.1]];
}

-(void)setCondition:(int)condition{
    
    if (condition == NoCondition) {

        [self addSubview:noConditionLabel];
        [progressView setFrame:CGRectZero];
        [self setBackgroundColor:[UIColor clearColor]];

    }else{
        [noConditionLabel removeFromSuperview];
        [self setupSelfBackground];
        
        float width = condition/100.0 * self.frame.size.width;
        [progressView setFrame:CGRectMake(0, 0, width, self.frame.size.height)];
    }
}

@end
