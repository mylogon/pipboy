//
//  ViewController.m
//  pipBoy
//
//  Created by Luke Sadler on 02/02/2015.
//  Copyright (c) 2015 Luke Sadler. All rights reserved.
//

#import "StatViewController.h"
#import "StatDetails.h"
#import "ViewManager.h"
#import "PBTableViewCell.h"
#import "StaticNoiseController.h"
#import "ScreenSetup.h"
#import <AudioToolbox/AudioToolbox.h>
#import "RadVibrate.h"


@implementation StatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Generates data
    [StatDetails sharedInstance];
    
}



@end
