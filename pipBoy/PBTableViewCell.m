//
//  PBTableViewCell.m
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "PBTableViewCell.h"
#import "Screen.h"
#import "ScreenSetup.h"

@interface PBTableViewCell ()
{
    UILabel * lvlLabel;
    UILabel * cellLabel;
}

@end

@implementation PBTableViewCell

+(NSString *)identifier { return @"cell"; }

-(instancetype)initWithStyle:(UITableViewCellStyle)style
             reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.backgroundColor = [UIColor clearColor];
        self.backgroundView.alpha = 0;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.layer.borderColor = [ScreenSetup greenColour].CGColor;
        self.layer.borderWidth = 0;
    }
    return self;
}

-(void)loadData:(RowObject *)object
    inCellWidth:(float)width{
    
    _rowObject = object;
    
    [self clear];
    
    const float bufferMult = 1.4;
    
    cellLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, width * 0.8, CGFLOAT_MAX)];
    [cellLabel setNumberOfLines:0];
    [cellLabel setText:object.title];
    [cellLabel setFont:[ScreenSetup monoFont]];
    [self.contentView addSubview:cellLabel];
    
    [cellLabel sizeToFit];
    
    float labelHeight = cellLabel.frame.size.height;
    int cellHeight = labelHeight * bufferMult;
    const float minCellHeight = 30;
    _rowObject.cellHeight = cellHeight > minCellHeight ? cellHeight : minCellHeight;
    
    [cellLabel setCenter:CGPointMake(cellLabel.center.x, _rowObject.cellHeight/2)];
    
    
    lvlLabel = [[UILabel alloc]initWithFrame:CGRectMake(width - 38, 0, 30, 20)];
        
    if (object.level > 0 || object.belongsToScreen.screenType == GeneralType) {
        [lvlLabel setCenter:CGPointMake(lvlLabel.center.x, cellLabel.center.y)];
        [lvlLabel setText:[NSString stringWithFormat:@"%d",object.level]];
        [lvlLabel setFont:[ScreenSetup monoFont]];
        [self.contentView addSubview:lvlLabel];
        [lvlLabel setTextAlignment:NSTextAlignmentRight];
    }
    
    [self changeLabels:^(UILabel* label){
        [label setTextColor:[ScreenSetup greenColour]];
    }];
}

-(void)clear{
    for (UIView * view in self.contentView.subviews) {
        [view removeFromSuperview];
        self.layer.borderWidth = 0;
    }
}

-(void)select{
    self.backgroundColor = [[ScreenSetup greenColour] colorWithAlphaComponent:.1];
    self.layer.borderWidth = 2;
    
//    [self changeLabels:^(UILabel * label){
//        [label setTextColor:[UIColor blackColor]];
//    }];
}

-(void)deselect{
    self.backgroundColor = [UIColor clearColor];
    self.layer.borderWidth = 0;
    
//    [self changeLabels:^(UILabel * label){
//        [label setTextColor:[ScreenSetup greenColour]];
//    }];
}

-(void)changeLabels:(void(^)(UILabel*))theLabel{
    for (UILabel * label in @[cellLabel,lvlLabel]) {
        theLabel(label);
    }
}

@end
