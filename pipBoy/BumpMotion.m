
#import <CoreMotion/CoreMotion.h>
#import "BumpMotion.h"
#import "AppDelegate.h"

#import "TorchController.h"
#import "StaticNoiseController.h"

typedef enum : NSUInteger {
    XDir,
    YDir,
    ZDir,
} Direction;

@interface BumpMotion() {

    CMMotionManager *motionManager;
    CADisplayLink *motionDisplayLink;
    
    NSDate * lastBump;
    
    float limit;
}

@end

@implementation BumpMotion

- (instancetype)init {

    if(self = [super init]) {
    
        motionManager = [[CMMotionManager alloc] init];
        motionManager.deviceMotionUpdateInterval = 0.04;
        
        motionDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(motionRefresh:)];
        [motionDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        if ([motionManager isDeviceMotionAvailable]) {
            [motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical];
        }
        
        self.pause = YES;
        
        lastBump = [NSDate date];
        
        limit = [(AppDelegate*)[UIApplication sharedApplication].delegate bumpSensitivity];
    }
    
    return self;
}

- (void)motionRefresh:(id)sender {
    
    double x = motionManager.deviceMotion.userAcceleration.x;
    double y = motionManager.deviceMotion.userAcceleration.y;
    double z = motionManager.deviceMotion.userAcceleration.z;
    
    if (fabs(x) > limit || fabs(y) > limit || fabs(z) > limit){
     
        lastBump = [NSDate date];
        
        NSTimeInterval timeGap = -[lastBump timeIntervalSinceNow];
        
        if (timeGap < .4  && _torchControllerDelegate.isTorchOn) {
            [_torchControllerDelegate bigBump];
            
        }else if([lastBump timeIntervalSinceNow] < 2){

            [_staticControllerDelegate bigBump];
        }
    }
}

@end
