//
//  StatDetails.m
//  pipBoy
//
//  Created by Luke Sadler on 07/02/2015.
//  Copyright (c) 2015 Luke Sadler. All rights reserved.
//

#import "StatDetails.h"
#import "ScreenData.h"

@interface StatDetails ()
{
    ScreenData * special;
    ScreenData * skills;
    ScreenData * perks;
    ScreenData * general;
}
@end

@implementation StatDetails

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static StatDetails *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(instancetype)init{
    if (self = [super init]) {
        [self setupSpecial];
        [self setupPerks];
        [self setupSkills];
        [self setupGeneral];
        
        _datas = super.datas;
        _datas = @[special,skills,perks,general];
    }
    return self;
}

-(void) setupSpecial{
  
    special = [ScreenData new];
    [special setTitle:@"S.P.E.C.I.A.L."];
    [special setType:SpecialType];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Strength";
        obj.imageName = obj.title; // This is kinda shit, but the names are already set. Laziness :/
        obj.info = @"Strength is a measure of your raw physical power. It affects how much you can carry, and determines the effectiveness of all melee attacks.";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Perception";
        obj.imageName = obj.title;
        obj.info = @"A high Perception grants a bonus to the Explosives, Lockpick, and Energy Weapons skills, and determines when red compass markings appear (which indicate threats).";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Endurance";
        obj.imageName = obj.title;
        obj.info = @"Endurance is a measure of your overall physical fitness. A high Endurance gives bonuses to health, environmental resistances, and the Big Guns and Unarmed skills.";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Charisma";
        obj.imageName = obj.title;
        obj.info = @"Having a high Charisma will improve people's disposition towards you, and give bonuses to both the Barter and Speech skills.";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Intelligence";
        obj.imageName = obj.title;
        obj.info = @"Intelligence affects the Science, Repair and Medicine skills. The higher your Intelligence, the more Skill Points you'll be able to distribute when you level up.";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Agility";
        obj.imageName = obj.title;
        obj.info = @"Agility affects your Small Guns and Sneak skills, and the number of Action Points available for V.A.T.S.";
    }];
    
    [special.objects newObject:^(RowObject * obj){
        obj.title = @"Luck";
        obj.imageName = obj.title;
        obj.info = @"Raising your Luck will raise all of your skills a little. Having high Luck will also improve your critical chance with all weapons.";
    }];
    
    [special setAllStatsToLevel:10];
}

-(void)setupSkills{
    
    skills = [ScreenData new];
    [skills setTitle:@"Skills"];
    [skills setType:SkillsType];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Barter";
        obj.imageName = obj.title;
        obj.info = @"The Barter skill affects the prices you get for buying and selling items. In general, the higher your Barter skill, the lower your prices on purchased items.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Big Guns";
        obj.imageName = obj.title;
        obj.info = @"The Big Guns skill determines your combat effectiveness with all oversized weapons such as the fat man, missile launcher, flamer, and gatling laser.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Energy Weapons";
        obj.imageName = obj.title;
        obj.info = @"The Energy Weapons skill determines your effectiveness with weapons such as the laser pistol, laser rifle, plasma rifle and plasma pistol.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Explosives";
        obj.imageName = obj.title;
        obj.info = @"The Explosives skill determines the power of set mines, the ease of disarming hostile mines, and the effectiveness of thrown grenades.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Lockpick";
        obj.imageName = obj.title;
        obj.info = @"The lockpick skill is used to open locked doors and containers.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Medicine";
        obj.imageName = obj.title;
        obj.info = @"The Medicine skill determines how many hit points you replenish when using a stimpak, and the effectiveness of Rad-X and RadAway.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Melee Weapons";
        obj.imageName = obj.title;
        obj.info = @"The melee weapons skill determines your effectiveness with melee weapons, from the simple lead pipe all the way up to the high-tech super sledge.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Repair";
        obj.imageName = obj.title;
        obj.info = @"The Repair skill allows you to maintain your weapons and apparel. In addition, the higher your Repair skill, the better the starting condition of any custom-made weapons.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Science";
        obj.imageName = obj.title;
        obj.info = @"The Science skill represents your combined scientific knowledge, and is primarily used to hack restricted computer terminals.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Small Guns";
        obj.imageName = obj.title;
        obj.info = @"The Small Guns skill determines your effectiveness with conventional projectile weapons, including the 10mm pistol, BB gun, assault rifle, and combat shotgun.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Sneak";
        obj.imageName = obj.title;
        obj.info = @"The higher your Sneak skill, the easier it is to remain undetected, steal an item, and pick someone's pocket. Also, successfully attacking while undetected grants an automatic critical hit.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Speech";
        obj.imageName = obj.title;
        obj.info = @"The Speech skill governs how much you can influence someone through dialogue and gain access to information they might otherwise not want to share.";
    }];
    
    [skills.objects newObject:^(RowObject * obj){
        obj.title = @"Unarmed";
        obj.imageName = obj.title;
        obj.info = @"The Unarmed skill is used for fighting without a weapon, or with the few weapons specifically designed for hand-to-hand combat, like brass knuckles and the power fist.";
    }];
    
    [skills setAllStatsToLevel:100];
}

-(void)setupPerks{
    perks = [ScreenData new];
    [perks setTitle:@"Perks"];
    [perks setType:PerksType];
    
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Daddy's Boy";
        obj.imageName = @"daddysBoy";
        obj.info = @"Just like dear old Dad, you've devoted your time to intellectual pursuits. You gain 5 points to your Science and Medicine skills. Requires IN 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Gun Nut (3)";
        obj.imageName = @"gunNut";
        obj.info = @"You're obsessed with using and maintaining a wide variety of conventional firearms. With each rank of the perk, you gain 5 points to your Small Guns and Repair skills. Requires AG 4, IN 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Intense Training (10)";
        obj.imageName = @"intenseTrainer";
        obj.info = @"With this perk, you can allocate another point to any of your S.P.E.C.I.A.L. attributes.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Lady Killer";
        obj.imageName = @"ladyKiller";
        obj.info = @"In combat, you do 10% extra damage against male (Black Widow) or female (Lady Killer) opponents. Outside of combat, you sometimes receive unique dialogue options when dealing with the opposite sex.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Little Leaguer (3)";
        obj.imageName = @"littleLeaguer";
        obj.info = @"Years as the Vault little league MVP have honed your hitting and throwing skills. With every rank of the perk, you gain 5 points in Melee Weapons and 5 points in Explosives. Requires ST 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Swift Learner (3)";
        obj.imageName = @"swiftLearner";
        obj.info = @"With each rank of this perk, you gain an additional 10% every time experience points are earned.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Thief (3)";
        obj.imageName = @"thief";
        obj.info = @"With each rank of this perk, you gain 5 points to your Sneak and Lockpick skills. Requires AG 4, PE 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Child at Heart";
        obj.imageName = @"childHeart";
        obj.info = @"This perk greatly improves your interactions with children, usually in the form of unique dialogue options. Requires CH 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Comprehension";
        obj.imageName = @"comprehension";
        obj.info = @"With the comprehension perk, you gain one additional skill point whenever you read a skill book. Requirs IN 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Educated";
        obj.imageName = @"educated";
        obj.info = @"With this perk, you gain three more skill points every time you advance a level. Requires IN 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Entomologist";
        obj.imageName = @"entomologist";
        obj.info = @"With the entomologist perk, you do +50% damage every time you attack mutated insects, such as the radroach, giant ant, and radscorpion. Requires IN 4, Science 40.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Iron Fist (3)";
        obj.imageName = @"ironFist";
        obj.info = @"For each rank of this perk, you do an additional 5 points of unarmed damage. Requires ST 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Scoundrel (3)";
        obj.imageName = @"scoundral";
        obj.info = @"Take the scoundrel perk and you can use your wily charms to influence people. Each rank raises your Speech and Barter skills by 5 points. Requires CH 4.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Bloody Mess";
        obj.imageName = @"bloodyMess";
        obj.info = @"With the bloody mess perk, characters and creatuers you kill often explode into a red, gut-ridden, eyeball-strewn paste. Fun! Oh, and you do 5% extra damage with all weapons.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Demolition Expert (3)";
        obj.imageName = @"demoExpert";
        obj.info = @"With each rank of this perk, all of your explosive weapons do an additional 20% damage. Requires 50 points in Explosives.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Fortune Finder";
        obj.imageName = @"fortuneFinder";
        obj.info = @"With the fortune finder perk, you uncover considerably more Nuka-Cola caps in containers than you normally would. Requires LU 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Gunslinger";
        obj.imageName = @"gunslinger";
        obj.info = @"When using a pistol or a similar one-handed weapon, your accuracy in V.A.T.S. increases significantly.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Lead Belly";
        obj.imageName = @"leadBelly";
        obj.info = @"With the lead belly perk, you take 50% less radiation damage every time you drink from an irradiated water source. Requires EN 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Toughness";
        obj.imageName = @"toughness";
        obj.info = @"With this perk, you gain +10% to overall damage resistance, up to a maximum of 85%. Requires EN 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Commando";
        obj.imageName = @"commando";
        obj.info = @"While using a rifle or a similar two-handed weapon, your accuracy in V.A.T.S. increases significantly.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Impartial Mediation";
        obj.imageName = @"impMediation";
        obj.info = @"With the impartial mediation perk, you gain an extra 30 points to your speech skill so long as you maintain a neutral karma level. Requires CH 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Rad Resistance";
        obj.imageName = @"radRes";
        obj.info = @"Rad resistance allows you to -- what else? -- resist radiation. This perk grants you an additional 25% to radiation resistance. Requires EN 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Scrounger";
        obj.imageName = @"scrounger";
        obj.info = @"With the scrounger perk, you find considerably more ammunition in containers than you normally would.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Size Matters (3)";
        obj.imageName = @"sizeMatters";
        obj.info = @"You're obsessed with really big weapons. With each rank of this perk, you gain an additional 15 points to your Big Guns skill. Requires EN 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Strong Back";
        obj.imageName = @"strongBack";
        obj.info = @"This perk allows you to carry an additional 50 pounds of equipment. Requires ST 5, EN 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Animal Friend (2)";
        obj.imageName = @"animalFriend";
        obj.info = @"At the first rank of this perk, animals simply won't attack you. At the second rank, they will eventually come to your aid in combat, but never against another animal. Affects dogs, yao guai, mole rats, and brahmin. Requires 6 CH.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Finesse";
        obj.imageName = @"finesse";
        obj.info = @"With the finesse perk, you have a higher chance to score a critical hit on an opponent in combat, the equivalent of having 5 extra points of Luck.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Here and Now";
        obj.imageName = @"hereNow";
        obj.info = @"This perk immediately grants an additional experience level, complete with all of the advantages that brings.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Mister Sandman";
        obj.imageName = @"mrSandman";
        obj.info = @"With this perk, when you're in sneak mode, you gain the option to silently kill any human or ghoul while they're sleeping. Plus, all such kills earn bonus XP. Requires sneak 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Mysterious Stranger";
        obj.imageName = @"mysteriousSt";
        obj.info = @"Gives you your own personal guardian angel... armed with a fully loaded .44 Magnum. With this perk, the Mysterious Stranger appears occasionally in V.A.T.S. mode to lend a hand, with deadly efficiency. Requires LU 6.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Nerd Rage";
        obj.imageName = @"nerd";
        obj.info = @"You've been pushed around long enough! With this perk, your strength is raised to 10 and you gain 50% to damage resistance whenever your health drops to 20% or below. Requires IN 5, Science 50.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Night Person";
        obj.imageName = @"nightPerson";
        obj.info = @"When the sun is down, a night person gains +2 to Intelligence and Perception, up to a maximum of 10. This perk directly affects your 'internal clock,' and it remains active both inside and outside.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Cannibal";
        obj.imageName = @"cannibal";
        obj.info = @"With the cannibal perk, when you're in sneak mode, you gain the option to eat a corpse to regain health. But every time you feed, you lose karma, and if the act is witnessed, it is considered a crime against nature.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Fast Metabolism";
        obj.imageName = @"fastMet";
        obj.info = @"With fast metabolism, you gain a 20% health bonus when using stimpaks.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Life Giver";
        obj.imageName = @"lifeGiver";
        obj.info = @"With this perk, you gain an additional 30 hit points. Requires EN 6";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Pyromaniac";
        obj.imageName = @"pyro";
        obj.info = @"With the pyromaniac perk, you do +50% damage with fire-based weapons, such as the flamer and shishkebab. Requires explosives 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Robotics Expert";
        obj.imageName = @"roboticsExpert";
        obj.info = @"With this perk, you do an additional 25% damage to any robot. Even better, sneaking up on a hostile robot undetected and activating it will put that robot into a permanent shutdown state. Requires science 50.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Silent Running";
        obj.imageName = @"silentRunning";
        obj.info = @"With the silent running perk, you gain an additional 10 points to Sneak, and running no longer factors into a successful sneak attempt. Requires AG 6, sneak 50.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Sniper";
        obj.imageName = @"sniper";
        obj.info = @"With this perk, your chance to hit an opponent's head in V.A.T.S. is significantly increased. Requires PE 6, AG 6.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Adamantium Skeleton";
        obj.imageName = @"adSkel";
        obj.info = @"With the adamantium skeleton perk, your limbs only receive 50% of the damage they normally would.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Chemist";
        obj.imageName = @"chemist";
        obj.info = @"With the chemist perk, the chems you take last twice as long. Requires Medicine 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Contract Killer";
        obj.imageName = @"contractKiller";
        obj.info = @"Becoming a contract killer means that any good character you kill will have an ear on their corpse, which you can then sell to a certain person (whose identity will be disclosed when you take the perk) for caps and negative karma.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Cyborg";
        obj.imageName = @"cyborg";
        obj.info = @"You've made permanent enhancements to your body! This perk instantly adds 10% to your damage, poison, and radiation resistances, and 10 points to your Energy Weapons skill. Requires science 60, Medicine 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Lawbringer";
        obj.imageName = @"lawBringer";
        obj.info = @"Becoming a lawbringer means that any evil character you kill will have a finger on their corpse, which you can then sell to a certain person (whose identity will be disclosed when you take the perk) for caps and positive karma.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Light Step";
        obj.imageName = @"lightStep";
        obj.info = @"With this perk, you never set off an enemy's mines or floor-based traps. Requires PE 6, AG 6.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Master Trader";
        obj.imageName = @"masterTrader";
        obj.info = @"When you become a master trader, the price of every item you buy from a vendor is reduced by 25%. Requires CH 6, barter 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Action Boy";
        obj.imageName = @"actionBoy";
        obj.info = @"With this perk, you gain an additional 25 action points to use in V.A.T.S. Requires AG 6.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Better Criticals";
        obj.imageName = @"betterCrits";
        obj.info = @"With the better criticals perk, you gain a 50% damage bonus every time a critical hit is scored on an opponent. Requires PE 6, LU 6.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Chem Resistant";
        obj.imageName = @"chemRes";
        obj.info = @"If you're chem resistant, that means you're 50% less likely to develop an addiction to chems like psycho and jet. Requires Medicine 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Tag!";
        obj.imageName = @"tag";
        obj.info = @"This perk allows you to tag a fourth skill, which instantly raises it by 15 points.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Computer Whiz";
        obj.imageName = @"computerWhiz";
        obj.info = @"Fail a hack attempt and get locked out of a computer? Not if you're a computer whiz! With this perk, you get a second chance at any computer you were previously locked out of. Requires IN 7, science 70.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Concentrated Fire";
        obj.imageName = @"concFire";
        obj.info = @"With concentrated fire, your accuracy to hit any body part in V.A.T.S. increases slightly with each subsequent hit on that body part. Requires Small Guns 60, Energy Weapons 60.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Infiltrator";
        obj.imageName = @"infiltr";
        obj.info = @"The infiltrator perk gives you a second attempt at a lock that you previously broke while trying to force it. Requires PE 7, Lockpick 70.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Paralyzing Palm";
        obj.imageName = @"paraPalm";
        obj.info = @"With paralyzing palm, you sometimes perform a special V.A.T.S. palm strike that paralyzes your opponent for 30 seconds. Note that in order to use the perk, you must be completely unarmed. Requires Unarmed 70.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Explorer";
        obj.imageName = @"explorer";
        obj.info = @"When you choose the explorer perk, every location in the world is revealed on your map. So get out there and explore!";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Grim Reaper's Sprint";
        obj.imageName = @"grimReaper";
        obj.info = @"With this perk, if you kill a target while in V.A.T.S., then all of your action points are restored upon exiting V.A.T.S. mode.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Ninja";
        obj.imageName = @"ninja";
        obj.info = @"The ninja perk grants you the power of the fabled shadow warriors. When attacking with melee or unarmed attacks, you gain a +15% critical chance on every strike. Sneak attack criticals do 25% more damage. Requires Sneak 80, Melee Weapons 80.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Solar Powered";
        obj.imageName = @"solarPower";
        obj.info = @"With the solar powered perk, you gain an additional 2 points to Strength when in direct sunlight, and slowly regenerate lost health. Requires EN 7.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Deep Sleep";
        obj.imageName = @"deepSleep";
        obj.info = @"You sleep deeply no matter where you are. You get the Well Rested benefit of +10% XP for eight hours no matter what bed you sleep in.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Puppies!";
        obj.imageName = @"puppies";
        obj.info = @"With the Puppies! perk, if Dogmeat dies, you'll be able to get a new canine companion from his litter of puppies. Just wait a bit, and you'll find your new furry friend waiting outside Vault 101.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Quantum Chemist";
        obj.imageName = @"quantChemist";
        obj.info = @"You have unlocked the secret to creating Nuka-Cola Quantum. With this perk, every ten Nuka-Colas in your inventory is immediately converted into a Nuka-Cola Quantum.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Devil's Highway";
        obj.imageName = @"devilsHighway";
        obj.info = @"When you choose the Devil's Highway perk, your Karma is instantly set to Very Evil.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Escalator to Heaven";
        obj.imageName = @"escToHeaven";
        obj.info = @"When you choose the Escalator to Heaven perk, your Karma is instantly set to Very Good.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Karma Rebalance";
        obj.imageName = @"karmaRebalance";
        obj.info = @"When you choose the Karmic Rebalance perk, your Karma is instantly set to Neutral.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"No Weakness";
        obj.imageName = @"noWeakness";
        obj.info = @"When you take the No Weakness perk, all S.P.E.C.I.A.L. statistics that are less than 5 are instantly increased to 5.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Nerves of Steel";
        obj.imageName = @"nervesOfSteel";
        obj.info = @"With the Nerves of Steel perk, you regenerate Action Points much more quickly than you normally would. Requires AG 7.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Rad Tolerance";
        obj.imageName = @"radTollerance";
        obj.info = @"Although you are still notified when you get Minor Radiation Poisoning, you do not suffer any ill effects from it. Requires EN 7.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Warmonger";
        obj.imageName = @"warMongerer";
        obj.info = @"You've figured out on your own how to build all the custom weapons! With the Warmonger perk, all custom weapon types become available to you without the necessary schematics. Requires IN 7.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Party Boy";
        obj.imageName = @"partyBoy";
        obj.info = @"You are such a Party Boy/Girl that you no longer suffer the withdrawal effect from alcohol addiction.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Rad Absorption";
        obj.imageName = @"radAbsorbtion";
        obj.info = @"With the Rad Absorption perk, your radiation level slowly decreases on its own over time. Requires EN 7.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Almost Perfect";
        obj.imageName = @"AlmostPerfect";
        obj.info = @"Take the Almost Perfect perk, and all S.P.E.C.I.A.L. statistics are instantly raised to 9.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Nuclear Anomaly";
        obj.imageName = @"nuclearAnomaly";
        obj.info = @"With the Nuclear Anomaly perk, whenever your Health is reduced to 20 or less, you will erupt into a devastating nuclear explosion. Note that any allies in the vicinity will also suffer the effects of the blast!";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Ant Might";
        obj.imageName = @"antMight";
        obj.info = @"Your body has been genetically enhanced with the strength and flame resistance of the Grayditch fire ants! Your Strength has increased by 1 and you are now 25% resistant to fire.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Ant Sight";
        obj.imageName = @"antSight";
        obj.info = @"Your body has been genetically enhanced with the perception and flame resistance of the Grayditch fire ants! Your Perception has increased by 1 and you are now 25% resistant to fire.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Barkskin";
        obj.imageName = @"barkSkin";
        obj.info = @"You've been exposed to Harold's mutation and your skin is now as hard as tree bark. As a result, you've gained a permanent +5% to damage resistance.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Dream Crusher";
        obj.imageName = @"dreamCrusher";
        obj.info = @"Something about your presence dampens others' desires to exceed. Any enemy's chance of getting critical hits on you is reduced by 50%";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Hematophage";
        obj.imageName = @"hema";
        obj.info = @"This perk will cause you to regain 20 health (instead of 1 health) from blood packs.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Power Armor Training";
        obj.imageName = @"pArmorTraining";
        obj.info = @"You have received the specialized training needed to move in any form of power armor.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Rad Regeneration";
        obj.imageName = @"radRegeneration";
        obj.info = @"After an experimental treatment, intense radiation keeps your body operating at peak performance regardless of crippling injuries... right up until death. As a result, when you suffer from advanced radiation poisoning, crippled limbs automatically regenerate.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Survival Assistant";
        obj.imageName = @"survivalAss";
        obj.info = @"You wrote the book on how to survive in the Wasteland, and have shared your secrets with humanity. This perk gives you +2% to poison and radiation resistance, plus one of the following: +5 hit points, +2 Medicine and +2 Science, +2 damage resistance, +2 Sneak and +2 Speech, or +1% critical chance.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Survival Expert";
        obj.imageName = @"survivalExp";
        obj.info = @"You wrote the book on how to survive in the Wasteland, and have shared your secrets with humanity. This perk gives you +4% to poison and radiation resistance, plus one of the following: +10 hit points, +4 medicine and +4 science, +4 damage resistance, +4 sneak and +4 speech, or +2% critical chance.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Survival Guru";
        obj.imageName = @"survivalGuru";
        obj.info = @"You wrote the book on how to survive in the Wasteland, and have shared your secrets with humanity. This perk gives you +6% to poison and radiation resistance, plus one of the following: +15 hit points, +6 Medicine and +6 Science, +6 damage resistance, +6 Sneak and +6 Speech, or +3% critical chance.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Wired Reflexes";
        obj.imageName = @"wiredReflexes";
        obj.info = @"Advanced technology from the Commonwealth has increased your reaction speed, giving you a higher chance to hit in V.A.T.S.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Auto Expert";
        obj.imageName = @"autoAxe";
        obj.info = @"You've become an expert with the Auto Axe. You do +25% damage whenever you use it. Vroom.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Booster Shot";
        obj.imageName = @"boosterShot";
        obj.info = @"Medical research from the Pitt has improved your life. Just try not to think of what it may have cost others. Your radiation resistance is improved by 10%.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Covert Ops";
        obj.imageName = @"covertOps";
        obj.info = @"You've recovered all the hidden intel from Operation: Anchorage. Good work, soldier!";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Ghoul Ecology";
        obj.imageName = @"ghoulEco";
        obj.info = @"You have learned to exploit the specific weaknesses of ghouls, and gain a +5 damage bonus when attacking one.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Pitt Fighter";
        obj.imageName = @"pittFighter";
        obj.info = @"The vicious fights in the Hole have left you stronger. Both your damage and radiation resistances have been increased by 3%.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Punga Power!";
        obj.imageName = @"pungaPower";
        obj.info = @"Behold the power of the Punga! The restorative effects of Punga fruit now have a greater effect on you.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Superior Defender";
        obj.imageName = @"superiorDefender";
        obj.info = @"With the Superior Defender perk, you gain +5 to your damage and +10 to your armor rating when standing still.";
    }];
    [perks.objects newObject:^(RowObject * obj){
        obj.title = @"Xenotech Expert";
        obj.imageName = @"xenoExp";
        obj.info = @"Your familiarity with alien technology gives you better control over their weapons, increasing their damage output by 20%.";
    }];
}

-(void)setupGeneral{
    general = [ScreenData new];
    [general setTitle:@"General"];
    [general setType:GeneralType];
    
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Quests Complete";
        obj.level = 57;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Locations Discovered";
        obj.level = 220;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"People Killed";
        obj.level = 1318;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Creatures Killed";
        obj.level = 2595;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Locks Picked";
        obj.level = 268;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Computers Hacked";
        obj.level = 123;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Stimpaks Taken";
        obj.level = 57;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Rad-X Taken";
        obj.level = 25;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"RadAway Taken";
        obj.level = 10;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Chems Taken";
        obj.level = 13;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Times Addicted";
        obj.level = 5;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Mines Disarmed";
        obj.level = 229;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Speech Successes";
        obj.level = 101;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Pockets Picked";
        obj.level = 5;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Pants Exploded";
        obj.level = 1;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Books Read";
        obj.level = 120;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Bobbleheads Found";
        obj.level = 20;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Weapons Created";
        obj.level = 72;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"People Mezzed";
        obj.level = 0;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Captives Rescued";
        obj.level = 10;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Sandman Kills";
        obj.level = 0;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Contracts Completed";
        obj.level = 0;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Corpses Eaten";
        obj.level = 0;
    }];
    [general.objects newObject:^(RowObject * obj){
        obj.title = @"Mysterious Stanger Visits";
        obj.level = 0;
    }];
}

@end
