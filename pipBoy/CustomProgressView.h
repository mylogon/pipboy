//
//  CustomProgressView.h
//  pipBoy
//
//  Created by Luke Sadler on 31/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProgressView : UIView

-(void)setCondition:(int)condition;

@end
