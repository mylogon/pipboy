//
//  ViewController.h
//  pipBoy
//
//  Created by Luke Sadler on 02/02/2015.
//  Copyright (c) 2015 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericViewController.h"

@interface StatViewController : GenericViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate,UITextFieldDelegate>

@end

