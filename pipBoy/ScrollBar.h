//
//  ScrollBar.h
//  pipBoy
//
//  Created by Luke Sadler on 03/01/2017.
//  Copyright © 2017 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollBar : UIView

-(void) tableScrolled:(float)progress;

@end
