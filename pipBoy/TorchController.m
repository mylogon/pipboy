//
//  TorchController.m
//  pipBoy
//
//  Created by Luke Sadler on 19/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "TorchController.h"
#import "ScreenSetup.h"

@implementation TorchController

UIView * baseView;
const float kDuration = 0.6;
UIView * torch;

-(instancetype)initWithView:(UIView *)touchView{
    if (self = [super init]) {
        baseView = touchView;
        _isTorchOn = false;
    }
    return self;
}

-(void)screenTapped:(UITapGestureRecognizer *)tap{
    
    _isTorchOn = !_isTorchOn;
    [SoundEffects playTorchOn:_isTorchOn];
    
    if (_isTorchOn) {
        [self torchOn];
        [_staticController pauseStaticLines];
    }else{
        [self torchOff];
        [_staticController resumeStaticLines];
    }
}

-(void)bigBump{
    //torch flicker
    
    UIColor * currentColour = torch.backgroundColor;
    
    float timings [2] = {0,0.15}; //make sure this stays an even number
    
    for (int i = 0; i < sizeof(timings)/sizeof(float); i++) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timings[i] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSLog(@"%i",i);
            if (i % 2 == 0) {
                //number even - turn light off
                [torch setBackgroundColor:[UIColor blackColor]];
            }else{
                [torch setBackgroundColor:currentColour];
            }
        });
    }
}

-(void)torchOn{
    torch = [[UIView alloc]initWithFrame:baseView.frame];
    [torch setBackgroundColor:[ScreenSetup torchColour]];
    [torch setAlpha:0];
    [baseView addSubview:torch];
    
    [UIView animateWithDuration:kDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [torch setAlpha:1];
                     }
                     completion:^(BOOL complete){
                         UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(screenTapped:)];
                         [tap setNumberOfTapsRequired:2];
                         [tap setNumberOfTouchesRequired:2];
                         [torch addGestureRecognizer:tap];
                     }];
    
}

-(void)torchOff{
    [UIView animateWithDuration:kDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [torch setAlpha:0];
                     }
                     completion:^(BOOL complete){
                         [torch removeFromSuperview];
                     }];
}

@end
