//
//  StatScreen.m
//  pipBoy
//
//  Created by Luke Sadler on 21/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "ScreenData.h"

@implementation ScreenData

-(instancetype)init{
    if (self=[super init]) {
        _objects = [[RowObjects alloc]initFromScreenData:self];
    }
    return self;
}

-(void)setAllStatsToLevel:(int)level{
    [self setAllTo:^(RowObject* obj){
        obj.level = level;
    }];
}

-(void)randomiseStatLevelsBetweenLevel:(int)lower
                             andHigher:(int)higher{
    [self setAllTo:^(RowObject* obj){
        int range = higher-lower;
        obj.level = arc4random()%range + lower;
    }];
}

-(void)setAllImageTo:(NSString *)imageName{
    [self setAllTo:^(RowObject* obj){
        obj.imageName = imageName;
    }];
}

-(void)setTheScreenType:(Screen *)screen{
    [self setAllTo:^(RowObject* obj){
        obj.belongsToScreen = screen;
    }];
}

-(void)setAllToDeselected{
    [self setAllTo:^(RowObject* obj){
        obj.isSelected = NO;
    }];
}

-(void)setAllToSelectable{
    [self setAllTo:^(RowObject* obj){
        obj.isSelectable = YES;
    }];
}

-(void)setAllTo:(void(^)(RowObject*))setTo{
    for (RowObject * obj in _objects.rowObjects) {
        setTo(obj);
    }
}

@end
