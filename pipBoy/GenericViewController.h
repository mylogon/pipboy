//
//  GenericViewController.h
//  pipBoy
//
//  Created by Luke Sadler on 01/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticNoiseController.h"
#import "Screen.h"

@interface GenericViewController : UIViewController


@end
