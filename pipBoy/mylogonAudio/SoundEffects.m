//
//  SoundEffects.m
//  pipBoy
//
//  Created by Luke Sadler on 01/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "SoundEffects.h"
#import "MylogonAudio.h"

@implementation SoundEffects


+(NSTimeInterval)playRandomStaticSound{
    
    return [[MylogonAudio sharedInstance] playSoundEffect:[NSString stringWithFormat:@"%d.wav",arc4random()&9]];
}

+(NSTimeInterval)playHorizontalSwitch{
    return [[MylogonAudio sharedInstance] playSoundEffect:@"horiz.wav"];
}

+(void)playSectionChange{
    [[MylogonAudio sharedInstance] playSoundEffect:@"tab.wav"];
}

+(void)playTableScroll{
    [[MylogonAudio sharedInstance] playSoundEffect:@"tableScroll.wav"];
}

+(void)playVerticalScreenScroll{
    [[MylogonAudio sharedInstance] playSoundEffect:@"select.wav"];
}

+(void)playNameSelect{
    [self playVerticalScreenScroll];
}

+(void)startRadSound{
    [[MylogonAudio sharedInstance] playBackgroundMusic:@"rad.mp3"];
}

+(void)stopRadSound{
    [[MylogonAudio sharedInstance] pauseBackgroundMusic];
}

+(void)playHum{
    float volume = [[NSUserDefaults standardUserDefaults] floatForKey:@"humVolume"];
    [[MylogonAudio sharedInstance]playSecondBackgroundMusic:@"hum.wav" volume:volume];
}

+(void)playAlert{
    [[MylogonAudio sharedInstance]playSoundEffect:@"popup.wav"];
}

+(void)playTextType{
    [self playTableScroll];
}

+(void)playTorchOn:(BOOL)on{
    [[MylogonAudio sharedInstance]playSoundEffect:on ? @"lightOn.wav" : @"lightOff.wav"];
}

@end
