//
//  SoundEffects.h
//  pipBoy
//
//  Created by Luke Sadler on 01/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundEffects : NSObject

/**returns the length of the sound file*/
+ (NSTimeInterval)playRandomStaticSound;
+ (NSTimeInterval)playHorizontalSwitch;

+(void)playTableScroll;
+(void)playSectionChange;
+(void)playVerticalScreenScroll;
+(void)playNameSelect;

+(void)startRadSound;
+(void)stopRadSound;

+(void)playHum;
+(void)playAlert;
+(void)playTextType;

+(void)playTorchOn:(BOOL)on;
@end
