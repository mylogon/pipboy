
#import <AVFoundation/AVFoundation.h>
#import "MylogonAudio.h"

@interface MylogonAudio()
@property (nonatomic) AVAudioPlayer *backgroundMusicPlayer;
@property (nonatomic) AVAudioPlayer *backgroundMusicPlayer2;

//Array to keep reference to player to stop ARC deallocating player before sound made. Work arround :/
@property (nonatomic) NSMutableArray <AVAudioPlayer*>* audioPlayers;
@end

@implementation MylogonAudio

+ (instancetype)sharedInstance {
    static MylogonAudio *sharedInstance;
    
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(instancetype)init{
    if (self = [super init]) {
        _audioPlayers = [NSMutableArray array];
    }
    return self;
}

- (void)playBackgroundMusic:(NSString *)filename {
    NSError *error;
    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.backgroundMusicPlayer.numberOfLoops = -1;
    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer play];
}

-(void)playBackgroundMusic:(NSString *)filename atTime:(NSTimeInterval)time{
    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:nil];
    self.backgroundMusicPlayer.numberOfLoops = -1;
    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer playAtTime:time];
    [self.backgroundMusicPlayer play];
    
}

-(void)playSecondBackgroundMusic:(NSString *)filename volume:(float)vol{
    
    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    self.backgroundMusicPlayer2 = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:nil];
    self.backgroundMusicPlayer2.numberOfLoops = -1;
    [self.backgroundMusicPlayer2 prepareToPlay];
    [self.backgroundMusicPlayer2 setVolume:vol];
    [self.backgroundMusicPlayer2 play];
    
}

-(void)pauseSecondBackgroundMusic{
    [self.backgroundMusicPlayer2 pause];
}

-(void)playBackgroundMusic:(NSString *)filename volume:(float)vol{
    
    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    self.backgroundMusicPlayer2 = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:nil];
    self.backgroundMusicPlayer2.numberOfLoops = -1;
    [self.backgroundMusicPlayer2 prepareToPlay];
    [self.backgroundMusicPlayer2 setVolume:vol];
    [self.backgroundMusicPlayer2 play];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [_audioPlayers removeObject:player];
}


- (void)pauseBackgroundMusic {
    [self.backgroundMusicPlayer pause];
}

- (void)resumeBackgroundMusic {
    [self.backgroundMusicPlayer play];
}

- (NSTimeInterval)playSoundEffect:(NSString*)filename {

    NSURL *soundEffectURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    
    AVAudioPlayer *soundEffectPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundEffectURL error:nil];
    [_audioPlayers addObject:soundEffectPlayer];
    soundEffectPlayer.numberOfLoops = 0;
    [soundEffectPlayer setDelegate:self];
    [soundEffectPlayer prepareToPlay];
    [soundEffectPlayer play];
    return soundEffectPlayer.duration;
}



@end
