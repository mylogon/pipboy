
#import <AVFoundation/AVFoundation.h>

@interface MylogonAudio : NSObject <AVAudioPlayerDelegate>

+ (instancetype)sharedInstance;

- (void)playBackgroundMusic:(NSString *)filename;
- (void)playBackgroundMusic:(NSString *)filename atTime:(NSTimeInterval)time;
- (void)playBackgroundMusic:(NSString *)filename volume:(float)vol;
- (void)playSecondBackgroundMusic:(NSString *)filename volume:(float)vol;

- (NSTimeInterval)playSoundEffect:(NSString*)filename;

- (void)pauseBackgroundMusic;
- (void)pauseSecondBackgroundMusic;

- (void)resumeBackgroundMusic;



@end
