//
//  ItemDetails.m
//  pipBoy
//
//  Created by Luke Sadler on 03/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "ItemDetails.h"
#import "ScreenData.h"
#import "ScreenEnums.h"

@interface ItemDetails ()
{
    ScreenData * weapons;
    ScreenData * apparel;
    ScreenData * aid;
    ScreenData * misc;
    ScreenData * ammo;
}
@end

@implementation ItemDetails

+(instancetype)sharedInstance{
    static ItemDetails * itemDetails = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        itemDetails = [ItemDetails new];
    });
    return itemDetails;
}

-(instancetype)init{
    if ([super init]) {
        [self setupWeapons];
        [self setupApparel];
        [self setupAid];
        [self setupMisc];
        [self setupAmmo];
        
        self.datas = @[weapons,apparel,aid,misc,ammo];
    }
    return self;
}

-(void)setupWeapons{
    weapons = [ScreenData new];
    [weapons setTitle:@"Weapons"];
    [weapons setType:WeaponsType];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"10mm Submachine Gun";
        obj.imageName = @"pistol10";
        obj.damage = 30;
        obj.weight = 5;
        obj.value = 132;
        obj.condition = 40;
        obj.takesAmmo = k10mm;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
       obj.title = @"Sniper Rifle";
        obj.imageName = @"sniperRifle";
        obj.damage = 40;
        obj.weight = 10;
        obj.value = 300;
        obj.condition = 30;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"A3-21's Plasma Rifle";
        obj.imageName = @"plasmaRifle";
        obj.damage = 44;
        obj.weight = 8;
        obj.value = 846;
        obj.condition = 60;
        obj.takesAmmo = kMFCell;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Alien Blaster";
        obj.imageName = @"alienBlaster";
        obj.damage = 91;
        obj.weight = 2;
        obj.value = 233;
        obj.condition = 65;
        obj.takesAmmo = kAlienPowerCell;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Blackhawk";
        obj.imageName = @"scoped44";
        obj.damage = 57;
        obj.weight = 4;
        obj.value = 460;
        obj.condition = 100;
        obj.takesAmmo = k44;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Chinese Assault Rifle";
        obj.imageName = @"assaultRifle";
        obj.damage = 50;
        obj.weight = 7;
        obj.value = 408;
        obj.condition = 96;
        obj.takesAmmo = k556;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Col. Autumn's Laser Pistol";
        obj.imageName = @"laserPistol";
        obj.damage = 70;
        obj.weight = 2;
        obj.value = 297;
        obj.condition = 80;
        obj.takesAmmo = kECell;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Fat Man";
        obj.imageName = @"fatMan";
        obj.damage = 1208;
        obj.weight = 30;
        obj.value = 401;
        obj.condition = 50;
        obj.takesAmmo = kMiniNuke;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Frag Granade (2)";
        obj.imageName = @"fragGranade";
        obj.damage = 106;
        obj.weight = 0.5;
        obj.value = 25;
        obj.condition = 100;
        obj.takesAmmo = kMiniNuke;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Frag Mine (5)";
        obj.imageName = @"fragMine";
        obj.damage = 106;
        obj.weight = 0.5;
        obj.value = 25;
        obj.condition = NoCondition;
        obj.takesAmmo = kNone;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Mesmetron";
        obj.imageName = @"mesmetron";
        obj.damage = 1;
        obj.weight = 2;
        obj.value = 499;
        obj.condition = 100;
        obj.takesAmmo = kMezPowerCell;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Missile Launcher";
        obj.imageName = @"missleLauncher";
        obj.damage = 130;
        obj.weight = 20;
        obj.value = 202;
        obj.condition = 55;
        obj.takesAmmo = kMissile;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Ol' Painless";
        obj.imageName = @"huntingRifle";
        obj.damage = 31;
        obj.weight = 6;
        obj.value = 239;
        obj.condition = 100;
        obj.takesAmmo = k32;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Shishkebab";
        obj.imageName = @"shishkebab";
        obj.damage = 34;
        obj.weight = 3;
        obj.value = 80;
        obj.condition = 75;
        obj.takesAmmo = kNone;
        obj.effects = @"HP -2(5s)";
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Smuggler's End";
        obj.imageName = @"laserPistol";
        obj.damage = 19;
        obj.weight = 2;
        obj.value = 450;
        obj.condition = 100;
        obj.takesAmmo = kECell;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"The Shocker";
        obj.imageName = @"powerFist";
        obj.damage = 30;
        obj.weight = 6;
        obj.value = 114;
        obj.condition = 90;
        obj.takesAmmo = kNone;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"The Terrible Shotgun";
        obj.imageName = @"combatShotgun";
        obj.damage = 75;
        obj.weight = 10;
        obj.value = 145;
        obj.condition = 80;
        obj.takesAmmo = kShell;
    }];
    
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Assault Rifle";
        obj.imageName = @"assaultRifle";
        obj.damage = 8;
        obj.weight = 7;
        obj.value = 300;
        obj.takesAmmo = k556;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Xuanlong Assault Rifle";
        obj.imageName = @"xuanlong";
        obj.damage = 12;
        obj.weight = 7;
        obj.value = 400;
        obj.takesAmmo = k556;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Lincoln's Repeater";
        obj.imageName = @"lincoln";
        obj.damage = 50;
        obj.weight = 5;
        obj.value = 500;
        obj.takesAmmo = k44;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Railway Rifle";
        obj.imageName = @"railwayRifle";
        obj.damage = 30;
        obj.weight = 9;
        obj.value = 200;
        obj.takesAmmo = kSpikes;
        obj.effects = @"3x limb damage";
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Hunting Rifle";
        obj.imageName = @"huntingRifle";
        obj.damage = 25;
        obj.weight = 6;
        obj.value = 150;
        obj.takesAmmo = k32;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"BB Gun";
        obj.imageName = @"bbGun";
        obj.damage = 4;
        obj.weight = 2;
        obj.value = 36;
        obj.takesAmmo = kBB;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Combat Shotgun";
        obj.imageName = @"combatShotgun";
        obj.damage = 55;
        obj.weight = 7;
        obj.value = 200;
        obj.takesAmmo = kShell;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Sawed-off shotgun";
        obj.imageName = @"sawedOff";
        obj.damage = 50;
        obj.weight = 6;
        obj.value = 190;
        obj.takesAmmo = kShell;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Flamer";
        obj.imageName = @"flamer";
        obj.damage = 16;
        obj.weight = 15;
        obj.value = 500;
        obj.takesAmmo = kFlamerFuel;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Gatling Laser";
        obj.imageName = @"gatlingLaser";
        obj.damage = 8;
        obj.weight = 18;
        obj.value = 2000;
        obj.takesAmmo = kElectron;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Minigun";
        obj.imageName = @"minigun";
        obj.damage = 5;
        obj.weight = 18;
        obj.value = 1000;
        obj.takesAmmo = k5mm;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Baseball Bat";
        obj.imageName = @"baseballBat";
        obj.damage = 9;
        obj.weight = 3;
        obj.value = 55;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Bottlecap Mine (8)";
        obj.imageName = @"bottlecapMine";
        obj.damage = 500;
        obj.weight = 0.5;
        obj.value = 75;
        obj.condition = NoCondition;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Pulse Mine (2)";
        obj.imageName = @"pulseMine";
        obj.damage = 10;
        obj.weight = 0.5;
        obj.value = 40;
        obj.condition = NoCondition;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Silenced 10mm Pistol";
        obj.imageName = @"silenced10";
        obj.damage = 8;
        obj.weight = 3;
        obj.value = 250;
        obj.takesAmmo = k10mm;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Dart Gun";
        obj.imageName = @"dartGun";
        obj.damage = 6;
        obj.weight = 3;
        obj.value = 500;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Ripper";
        obj.imageName = @"ripper";
        obj.damage = 30;
        obj.weight = 6;
        obj.value = 100;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Power Fist";
        obj.imageName = @"powerFist";
        obj.damage = 20;
        obj.weight = 6;
        obj.value = 100;
        obj.takesAmmo = kNone;
    }];
    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Deathclaw Gauntlet";
        obj.imageName = @"deathclaw";
        obj.damage = 20;
        obj.weight = 10;
        obj.value = 150;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Nuka-Granade";
        obj.imageName = @"nukaGranade";
        obj.damage = 500;
        obj.weight = 0.5;
        obj.value = 50;
        obj.condition = NoCondition;
        obj.takesAmmo = kNone;
    }];

    [weapons.objects newObject:^(RowObject * obj){
        obj.title = @"Rock-It Launcher";
        obj.imageName = @"rockitLauncher";
        obj.damage = 50;
        obj.weight = 8;
        obj.value = 200;
        obj.takesAmmo = kRockItLauncher;
    }];
    
    [weapons.objects sortObjectsAlpha];
    
    [weapons.objects randomSetCondition];
    
}

-(void)setupApparel{
    apparel = [ScreenData new];
    [apparel setTitle:@"Apparel"];
    [apparel setType:ApparelType];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Armored Vault 101 Jumpsuit";
        obj.imageName = @"armoredVault";
        obj.damage = 8;
        obj.weight = 15;
        obj.value = 72;
        obj.condition = 50;
        obj.effects = @"Sm. Guns. +5, Ener. Weap. +5";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Brotherhood Power Armor";
        obj.imageName = @"powerArmor";
        obj.damage = 40;
        obj.weight = 45;
        obj.value = 722;
        obj.condition = 100;
        obj.effects = @"STR +2, AGL -2, Rad. Res. +10";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Brotherhood Power Helmet";
        obj.imageName = @"powerHelmet";
        obj.damage = 8;
        obj.weight = 5;
        obj.value = 106;
        obj.condition = 100;
        obj.effects = @"Rad. Res. +3";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Ghoul Mask";
        obj.imageName = @"ghoulMask";
        obj.damage = 3;
        obj.weight = 1;
        obj.value = 40;
    }];

    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Sunglasses";
        obj.imageName = @"sunglasses";
        obj.damage = 1;
        obj.weight = 0;
        obj.value = 5;
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Armored Vault 101 Suit";
        obj.imageName = @"armoredVault";
        obj.damage = 12;
        obj.weight = 15;
        obj.value = 180;
        obj.effects = @"Ener. Weapons +5, Sm. Guns +5";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"AntAgonizer's Costume";
        obj.imageName = @"antagoniser";
        obj.damage = 20;
        obj.weight = 5;
        obj.value = 60;
        obj.effects = @"CHR. -1, AGIL +1";
    }];

    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Chinese Jumpsuit";
        obj.imageName = @"chinese";
        obj.damage = 6;
        obj.weight = 2;
        obj.value = 6;
        obj.effects = @"Sm. Guns +5";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Advanced Radiation Suit";
        obj.imageName = @"radiationSuit";
        obj.damage = 8;
        obj.weight = 7;
        obj.value = 100;
        obj.effects = @"Rad. Res. +40";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Naughty Nightware";
        obj.imageName = @"pajamas";
        obj.damage = 1;
        obj.weight = 1;
        obj.value = 200;
        obj.effects = @"LUCK +1, Speach +10";
    }];

    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Mysterious Stranger Outfit";
        obj.imageName = @"vault101";
        obj.damage = 5;
        obj.weight = 3;
        obj.value = 40;
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Sheriff's Duster";
        obj.imageName = @"cowboyDuster";
        obj.damage = 5;
        obj.weight = 3;
        obj.value = 35;
        obj.effects = @"CHR. +1, Sm. Guns. +5, Guns +5";
    }];
    
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Power Armor";
        obj.imageName = @"powerArmor";
        obj.damage = 40;
        obj.weight = 45;
        obj.value = 739;
        obj.effects = @"STR. +2, AGIL. -2, Rad. Res. +10";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Boogeyman's Hood";
        obj.imageName = @"boogeyman";
        obj.damage = 8;
        obj.weight = 3;
        obj.value = 110;
    }];
    
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Eyeglasses";
        obj.imageName = @"eyeglasses";
        obj.damage = 1;
        obj.value = 8;
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Tunnel Snake Outfit";
        obj.imageName = @"tunnelSnake";
        obj.damage = 4;
        obj.weight = 2;
        obj.value = 8;
        obj.effects = @"Mel. Weap. +5";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Vault 101 Security Armor";
        obj.imageName = @"vaultSecurity";
        obj.damage = 12;
        obj.weight = 15;
        obj.value = 70;
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Vault Lab Uniform";
        obj.imageName = @"vaultLab";
        obj.damage = 2;
        obj.weight = 1;
        obj.value = 6;
        obj.effects = @"Sci. +5";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Combat Armor";
        obj.imageName = @"combatArmor";
        obj.damage = 32;
        obj.weight = 25;
        obj.value = 390;
    }];
    
    //cut content
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Sim Regen Armor";
        obj.imageName = @"combatArmor";
        obj.damage = 32;
        obj.weight = 25;
        obj.value = 390;
        obj.effects = @"Health regeneration";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Prototype Medic Power Armor";
        obj.imageName = @"powerArmor";
        obj.damage = 40;
        obj.weight = 45;
        obj.value = 1000;
        obj.effects = @"Rad. Res. +25%, -1 AGIL";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Wasteland Outfit";
        obj.imageName = @"wastelands";
        obj.damage = 2;
        obj.weight = 2;
        obj.value = 6;
        obj.effects = @"AGIL. +1, END. +1";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Raider Armor";
        obj.imageName = @"raiderBlastmaster";
        obj.damage = 16;
        obj.weight = 15;
        obj.value = 180;
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Tesla Armor";
        obj.imageName = @"teslaArmor";
        obj.damage = 43;
        obj.weight = 45;
        obj.value = 820;
        obj.effects = @"Ener. Weap. +10, Rad. Res. +20";
    }];
    
    [apparel.objects newObject:^(RowObject * obj){
        obj.title = @"Vault 100 Jumpsuit";
        obj.imageName = @"vault100";
        obj.damage = 1;
        obj.weight = 1;
        obj.value = 6;
        obj.effects = @"Mel. Weap. +2, SP +2";
    }];
    [apparel.objects sortObjectsAlpha];
    [apparel.objects randomSetCondition];
}

-(void)setupAid{
    aid = [ScreenData new];
    [aid setTitle:@"Aid"];
    [aid setType:AidType];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Buffout (4)";
        obj.imageName = @"buffout";
        obj.weight = 0;
        obj.value = 20;
        obj.effects = @"HP +60, END +3, STR +2";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Jet (2)";
        obj.imageName = @"jet";
        obj.value = 20;
        obj.effects = @"AP +30";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Mentats (6)";
        obj.imageName = @"mentats";
        obj.value = 20;
        obj.effects = @"PER +5, INT +5";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Nuka-Cola Quantum (3)";
        obj.imageName = @"nukaCola";
        obj.weight = 1;
        obj.value = 20;
        obj.effects = @"Rads +6, AP +20";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Orange Mentats";
        obj.imageName = @"mentats";
        obj.value = 20;
        obj.effects = @"PER +5";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Psycho (4)";
        obj.imageName = @"psycho";
        obj.value = 20;
        obj.effects = @"Damage +25%";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Purified Water (8)";
        obj.imageName = @"water";
        obj.weight = 1;
        obj.value = 20;
        obj.effects = @"HP +20";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Rad-X (7)";
        obj.imageName = @"radX";
        obj.value = 20;
        obj.effects = @"Rad. Res. +75";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"RadAway (20)";
        obj.imageName = @"radAway";
        obj.value = 20;
        obj.effects = @"Rads -150";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Stealth Boy (18)";
        obj.imageName = @"stealthBoy";
        obj.weight = 1;
        obj.value = 100;
        obj.effects = @"Sneak +100, Stealth Field +75";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Stimpak (180)";
        obj.imageName = @"stimpak";
        obj.value = 25;
        obj.effects = @"HP +108";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Vodka";
        obj.imageName = @"vodka";
        obj.weight = 1;
        obj.value = 20;
        obj.effects = @"CHR +1,INT -1,STR +1";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Whiskey";
        obj.imageName = @"wiskey";
        obj.weight = 1;
        obj.value = 10;
        obj.effects = @"CHR +1,INT -1,STR +1";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
        obj.title = @"Wine";
        obj.imageName = @"wine";
        obj.weight = 1;
        obj.value = 10;
        obj.effects = @"CHR +1, INT -1,STR +1";
    }];
    
    [aid.objects newObject:^(RowObject * obj){
       obj.title = @"Beer";
        obj.imageName = @"beer";
        obj.weight = 1;
        obj.value = 2;
        obj.effects = @"+1 CH, -1 IN, +1 ST";
    }];
    
    
    
    [aid.objects sortObjectsAlpha];
}

-(void)setupMisc{
    misc = [ScreenData new];
    [misc setTitle:@"Misc"];
    [misc setType:MiscType];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Abraxo Cleaner";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 5;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Bobby Pin (43)";
        obj.imageName = @"bobbyPin";
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Bottlecap (3000)";
        obj.imageName = @"bottlecap";
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Brotherhood of Steel Holotag (13)";
        obj.imageName = @"holotag";
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
       obj.title = @"Ashtray";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Baseball";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 2;
    }];
   
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Basketball";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Clipboard";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Finger (64)";
        obj.imageName = @"finger";
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Food Sanitiser";
        obj.imageName = @"foodSanitiser";
        obj.weight = 7;
        obj.value = 150;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"House: Jukebox";
        obj.imageName = @"home";
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"House: My First Infirmary";
        obj.imageName = @"home";
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"House: Nuka-Cola Machine";
        obj.imageName = @"home";
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"House: Pre-War Theme";
        obj.imageName = @"home";
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"House: Workbench";
        obj.imageName = @"home";
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Lucky 8 Ball";
        obj.imageName = @"8Ball";
        obj.weight = 1;
        obj.value = 4;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Lunchbox";
        obj.imageName = @"lunchbox";
        obj.weight = 1;
        obj.value = 3;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Medical Brace";
        obj.imageName = @"junk";
        obj.weight = 2;
        obj.value = 10;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Pre-War Book (6)";
        obj.imageName = @"book";
        obj.weight = 1;
        obj.value = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Sensor Module";
        obj.imageName = @"junk";
        obj.weight = 2;
        obj.value = 30;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Slave Collar";
        obj.imageName = @"collar";
        obj.weight = 1;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Surgical Tubing";
        obj.imageName = @"junk";
        obj.weight = 1;
        obj.value = 10;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Yew's Bear Charm";
        obj.imageName = @"bear";
        obj.weight = 1;
        obj.value = 50;
    }];
    
    [misc.objects newObject:^(RowObject * obj){
        obj.title = @"Keyring";
        obj.imageName = @"keyring";
    }];
    
    [misc.objects sortObjectsAlpha];
}

-(void)setupAmmo{
    ammo = [ScreenData new];
    [ammo setTitle:@"Ammo"];
    [ammo setType:AmmoType];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k308];
        obj.imageName = @"308";
        obj.value = 3;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k32];
        obj.imageName = @"32";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k44];
        obj.imageName = @"44";
        obj.value = 2;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k10mm];
        obj.imageName = @"10";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k556];
        obj.imageName = @"556";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:k5mm];
        obj.imageName = @"5";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kAlienPowerCell];
        obj.imageName = @"alienPowerCell";
        obj.value = 10;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kDart];
        obj.imageName = @"dart";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kElectron];
        obj.imageName = @"electronChargePack";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kECell];
        obj.imageName = @"energyCell";
        obj.value = 2;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kFlamerFuel];
        obj.imageName = @"flamerFuel";
        obj.value = 1;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kMezPowerCell];
        obj.imageName = @"energyCell";
        obj.value = 10;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kMFCell];
        obj.imageName = @"electronChargePack";
        obj.value = 3;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kMiniNuke];
        obj.imageName = @"miniNuke";
        obj.value = 250;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kMissile];
        obj.imageName = @"missile";
        obj.value = 50;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kSpikes];
        obj.imageName = @"railwaySpike";
        obj.value = 2;
    }];
    
    [ammo.objects newObject:^(RowObject * obj){
        obj.title = [AmmoTypes getAmmoTitle:kShell];
        obj.imageName = @"shell";
        obj.value = 2;
    }];
    
    [ammo.objects sortObjectsAlpha];
}

@end
