//
//  ViewManager.h
//  pipBoy
//
//  Created by Luke Sadler on 24/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Screen.h"
#import "ScreenEnums.h"


@interface ViewManager : NSObject

+(ViewManager*)sharedManager;

//CND stat screen
-(Screen *)getInitialScreen;

-(void)receivedSwipe:(UISwipeGestureRecognizer *)swipe
     shouldMakeNoise:(void(^)(ScreenChangeType))makeNoise
        assignScreen:(void(^)(Screen*))screen;

@end
