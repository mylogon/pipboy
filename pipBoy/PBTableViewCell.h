//
//  PBTableViewCell.h
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RowObject.h"

@interface PBTableViewCell : UITableViewCell

+(NSString*)identifier;

-(void)loadData:(RowObject *)object inCellWidth:(float)width;
-(void)clear;

-(void)select;
-(void)deselect;

@property (readonly)RowObject * rowObject;

@end
