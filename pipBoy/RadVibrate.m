//
//  RadVibrate.m
//  pipBoy
//
//  Created by Luke Sadler on 29/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "RadVibrate.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation RadVibrate

bool playing;


-(instancetype)init{
    if (self = [super init]) {
        playing = NO;
    }
    return self;
}


-(void)start{
    playing = YES;
    [self vibrate];
}

-(void)stop{
    playing = NO;
}

-(void)vibrate{
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    if (playing) {
        [self performSelector:@selector(vibrate) withObject:nil afterDelay:(arc4random()%20)/10];
    }
}

@end
