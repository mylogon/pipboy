//
//  StaticNoiseController.m
//  pipBoy
//
//  Created by Luke Sadler on 26/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "StaticNoiseController.h"
#import "StatViewController.h"
#import "ScreenSetup.h"
#import "TheViewController.h"

@interface UIImageView (bottom)
-(void)setFrameToBottomOfScreen;
@end
@implementation UIImageView (bottom)

-(void)setFrameToBottomOfScreen{
    
    UIWindow * currentWindow = [UIApplication sharedApplication].keyWindow;
    CGRect newRect = self.frame;
    newRect.origin.y = currentWindow.frame.size.height;
    [self setFrame:newRect];
}

@end

@interface StaticNoiseController ()
{
    TheViewController * viewController;
    UIView * theTouchView;
    
    BOOL isPaused;
}
@end

@implementation StaticNoiseController


-(instancetype)initWithViewController:(TheViewController*)vc andTouchLayer:(UIView*)touchView{
    if (self = [super init]) {
        viewController = vc;
        theTouchView = touchView;
        [self createRandomTimer];
        
    }
    return self;
}

-(void)pauseStaticLines{
    isPaused = true;
}

-(void)resumeStaticLines{
    isPaused = false;
}

-(void)createRandomTimer{
    //somewhere between 30 - 60 seconds
    [self performSelector:@selector(createRandomStatic) withObject:nil afterDelay:(arc4random()%30) + 30];
}

-(void)createRandomStatic{
    [self createRandomTimer];
    
    if (isPaused) return;
    
    // 30% chance .. i think
    if ((arc4random()%10)%4 == 0) {
        [self makeSingleQuiteStatic];
    }else{
        [self makeSingleStatic:NO];
    }
}

-(UIImageView*)getStaticLine{
    UIImage * scanImage = [UIImage imageNamed:@"pipboyscanlines"];
    UIImageView * staticMark = [[UIImageView alloc]initWithImage:scanImage];
    [staticMark setFrame:CGRectMake(0, -scanImage.size.height, viewController.view.frame.size.width, scanImage.size.height*1.5)];
    
    [viewController.view insertSubview:staticMark
                          belowSubview:theTouchView];
    [staticMark setTintColor:[ScreenSetup greenColour]];
    
    return staticMark;
}

//Quiet + slow
-(void)makeSingleQuiteStatic{
    UIImageView * scanLine = [self getStaticLine];
    [viewController.view addSubview:scanLine];
    
    [UIView animateWithDuration:5
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0.05
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [scanLine setFrameToBottomOfScreen];
                     }
                     completion:^(BOOL complete){
                         [scanLine removeFromSuperview];
                     }];
}


//Noisy
-(NSTimeInterval)makeSingleStatic:(BOOL)horizonal{
    
    NSTimeInterval staticSoundTime;
    if (horizonal) {
        staticSoundTime = [SoundEffects playHorizontalSwitch];
    }else{
        staticSoundTime = [SoundEffects playRandomStaticSound];
    }
    
    if (!horizonal) {
        
        UIImageView * staticMess = [[UIImageView alloc]initWithFrame:viewController.view.frame];
        [staticMess setImage:[UIImage imageNamed:@"staticMess"]];
        [staticMess setAlpha:0];
        
        [viewController.view insertSubview:staticMess
                              belowSubview:theTouchView];
        
        [UIView animateWithDuration:staticSoundTime/2
                              delay:0
                            options:UIViewAnimationOptionAutoreverse
                         animations:^{
                             [staticMess setAlpha:0.4];
                         }
                         completion:^(BOOL complete){
                             [staticMess setAlpha:1];
                             [staticMess removeFromSuperview];
                         }];
    }
    
    /////
    
    
    UIImageView * staticMark = [self getStaticLine];
    UIImageView * staticMark2 = [self getStaticLine];
    
    [viewController.view addSubview:staticMark];
    
    UIViewAnimationOptions options = horizonal ? UIViewAnimationOptionCurveLinear : UIViewAnimationOptionAutoreverse;
    
    [UIView animateWithDuration:staticSoundTime/2
                          delay:0
                        options:options
                     animations:^{
                         
                         [staticMark setFrameToBottomOfScreen];
                     }
                     completion:^(BOOL complete){
                         [staticMark removeFromSuperview];
                     }];
    
    int r = arc4random()%10;
    bool showSecond = r%4;
    
    if (showSecond && !horizonal) {
        [viewController.view addSubview:staticMark2];
        
        [UIView animateWithDuration:staticSoundTime/2
                              delay:staticSoundTime*.2
                            options:UIViewAnimationOptionAutoreverse | UIModalTransitionStyleCrossDissolve
                         animations:^{
                             
                             CGRect newRect = staticMark2.frame;
                             newRect.origin.y = arc4random()%(int)viewController.view.frame.size.height/2;
                             [staticMark2 setFrame: newRect];
                         }
                         completion:^(BOOL complete){
                             [staticMark2 removeFromSuperview];
                         }];
    }else{
        [staticMark2 removeFromSuperview];
    }
    return staticSoundTime;
}


-(void)bigBump{
    NSLog(@"Big bump");
    
    CGRect offsetRect = viewController.view.frame;
    offsetRect.origin.y -= offsetRect.size.height;
    UIImage * image = [self getTheScreenShotFromWindow];
    
    NSTimeInterval length = [self makeSingleStatic:NO];
    float breakUp = .2;
    int splitUp = length/breakUp;
    
    for (int i = 0; i < splitUp ;i++){
    
        UIImageView * copy = [[UIImageView alloc]initWithImage:image];
        [copy setFrame:offsetRect];
        [viewController.view addSubview:copy];
        
        [UIView animateWithDuration:breakUp
                         animations:^{
                             [copy setFrame:viewController.view.frame];
                         }completion:^(BOOL complete){
                             [copy removeFromSuperview];
                         }];
    }
}

- (UIImage *)getTheScreenShotFromWindow
{
    // get the window size
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    CGSize size = window.frame.size;
    
    // check the scale - retina or regular
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext(size);
    
    // get the context
    [window.rootViewController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // save the context to image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // return the image
    return image;
}


@end
