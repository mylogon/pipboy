//
//  TheViewController.m
//  pipBoy
//
//  Created by Luke Sadler on 01/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "TheViewController.h"
#import "StaticNoiseController.h"
#import "GenericViewController.h"
#import "ViewManager.h"
#import "StatViewController.h"
#import "ScreenSetup.h"
#import "ScreenEnums.h"
#import "TorchController.h"
#import "BumpMotion.h"

@interface TheViewController () <UITextFieldDelegate>
{
    SectionsAvailable section;
    GenericViewController * currentSectionViewController;
    Screen * mainView;
    BumpMotion * bump;
}
@property (nonatomic) TorchController * torch;
@property (nonatomic) UIView * touchScreen;
@property (nonatomic) StaticNoiseController * staticNoise;
@end

NSString *const kLevelString = @" - Level 30";
NSString *const kNameKey = @"name";
const int kNameTag = 1;

@implementation TheViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentSectionViewController = [StatViewController new];
    
    [self.view addSubview:currentSectionViewController.view];
    
    mainView = [[ViewManager sharedManager]getInitialScreen];
    
    [currentSectionViewController.view addSubview:mainView];
    
    _staticNoise = [[StaticNoiseController alloc]initWithViewController:self
                                                          andTouchLayer:_touchScreen];
    
    _torch = [[TorchController alloc]initWithView:self.view];
    [_torch setStaticController:_staticNoise];
    
    bump = [[BumpMotion alloc]init];
    [bump setPause:NO];
    [bump setTorchControllerDelegate:_torch];
    [bump setStaticControllerDelegate:_staticNoise];
    
    [self setupGestures];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    NSString * name = [[NSUserDefaults standardUserDefaults]objectForKey:kNameKey];
    
    if (name) {
        [self setNameLabel:name];
    }else{
        [self namePopup:nil];
    }
}

-(void)setupGestures{

    //s1-4 = screen change. sec1-2 section change
    UISwipeGestureRecognizer *s1 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    UISwipeGestureRecognizer *s2 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    UISwipeGestureRecognizer *s3 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    UISwipeGestureRecognizer *s4 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    UISwipeGestureRecognizer *sec1 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    UISwipeGestureRecognizer *sec2 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeAction:)];
    s1.direction = UISwipeGestureRecognizerDirectionLeft;
    s2.direction = UISwipeGestureRecognizerDirectionRight;
    s3.direction = UISwipeGestureRecognizerDirectionUp;
    s4.direction = UISwipeGestureRecognizerDirectionDown;
    sec1.direction = UISwipeGestureRecognizerDirectionLeft;
    sec2.direction = UISwipeGestureRecognizerDirectionRight;
    [sec1 setNumberOfTouchesRequired:SectionSwipe];
    [sec2 setNumberOfTouchesRequired:SectionSwipe];
    
    UITapGestureRecognizer * torch = [[UITapGestureRecognizer alloc]initWithTarget:_torch action:@selector(screenTapped:)];
    [torch setNumberOfTouchesRequired:TorchTouch];
    [torch setNumberOfTapsRequired:2];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(screenTapped:)];
    [self.view addGestureRecognizer:tap];
    
    _touchScreen = [[UIView alloc]initWithFrame:self.view.frame];
    _touchScreen.backgroundColor = [UIColor clearColor];
    
    for (UIGestureRecognizer * gr in @[s1,s2,s3,s4,sec1,sec2,tap,torch]) {
        [_touchScreen addGestureRecognizer:gr];
    }
    
    [self.view addSubview:_touchScreen];
}

-(void)screenTapped:(UITapGestureRecognizer*)tap{
    
    CGPoint tapPoint = [tap locationInView:mainView];
    
    if (mainView.screenType == CndVert) {
        //name label
        UILabel * name = [mainView viewWithTag:kNameTag];
        if (CGRectContainsPoint(name.frame, tapPoint)) {
            [self namePopup:[name.text stringByReplacingOccurrencesOfString:kLevelString withString:@""]];
        }
    }
    
    
    if (mainView.table) {
        CGRect tableTopRect = CGRectMake(0, 0, mainView.table.frame.size.width, 40);
        
        if (CGRectContainsPoint(tableTopRect, tapPoint)) {
            [mainView tappedTop];
        }
    }
}

typedef void (^SwipeSwap)(void);

- (void)checkSwipeInvert:(UISwipeGestureRecognizer*)swipe{
    
    SwipeSwap horizSwap = ^{
        if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
            swipe.direction = UISwipeGestureRecognizerDirectionRight;
        }else if (swipe.direction == UISwipeGestureRecognizerDirectionRight){
            swipe.direction = UISwipeGestureRecognizerDirectionLeft;
        }
    };
    
    SwipeSwap vertSwap = ^{
        if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
            swipe.direction = UISwipeGestureRecognizerDirectionDown;
        }else if (swipe.direction == UISwipeGestureRecognizerDirectionDown){
            swipe.direction = UISwipeGestureRecognizerDirectionUp;
        }
    };
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    bool horizSectionInvert = [defaults boolForKey:@"horizInvertSection"];
    bool horizScreenInvert = [defaults boolForKey:@"horizInvertScreen"];
    bool vertInvert = [defaults boolForKey:@"vertInvert"];
    
    if (swipe.numberOfTouches == ScreenSwipe) {
        if (horizScreenInvert) {
            horizSwap();
        }
        
        if (vertInvert) {
            vertSwap();
        }
    }else if (swipe.numberOfTouchesRequired == SectionSwipe){
        if (horizSectionInvert) {
            horizSwap();
        }
    }
}

- (void)swipeAction:(UISwipeGestureRecognizer*)swipe{
    
    //Warning:- this function changes swipe's reference value. Inverting the swipe direction needs to be undone, which happens at the end of this function
    [self checkSwipeInvert:swipe];
    
    __block ScreenChangeType type;
    
    [[ViewManager sharedManager]receivedSwipe:swipe
                              shouldMakeNoise:^(ScreenChangeType screenChangeType){
                                  
                                  type = screenChangeType;
                                  
                                  if (type == VerticalChange) {
                                      [SoundEffects playVerticalScreenScroll];
                                  }else if (type == SectionChange){
                                      [SoundEffects playSectionChange];
                                  }
                              }
                                 assignScreen:^(Screen* screen){
                                     
                                     if (screen) {
                                         
                                         [mainView removeFromSuperview];
                                         mainView = screen;
                                         [self.view insertSubview:mainView
                                                     belowSubview:self.touchScreen];
                                     }
                                     
                                     //delayed after a screen change so that the visual effect is on new screen
                                     if (type == HorizonalChange || type == SectionChange) {
                                         [self.staticNoise makeSingleStatic:YES];
                                     }
                                     
                                     if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
                                         [mainView swipeUp];
                                     }else if (swipe.direction == UISwipeGestureRecognizerDirectionDown){
                                         [mainView swipeDown];
                                     }
                                     
                                     if (mainView.screenType == RADType) {
                                         [SoundEffects startRadSound];
                                     }else{
                                         [SoundEffects stopRadSound];
                                     }
                                 }
     ];
    
    // Hello. Reversion done here. Just run it though this function again to undo the damage
    [self checkSwipeInvert:swipe];
}

-(void) setNameLabel:(NSString*)name{
    static UILabel * nameLabel = nil;
    //in case name is changed
    [nameLabel removeFromSuperview];
    
    nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 500, 100)];
    [nameLabel setTag:kNameTag];
    [nameLabel setFont:[UIFont fontWithName:@"monofonto" size:15]];
    [nameLabel setTextColor:[ScreenSetup greenColour]];
    nameLabel.text = [NSString stringWithFormat:@"%@%@",name,kLevelString];
    [nameLabel sizeToFit];
    [nameLabel setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height * 0.85)];
    
    [mainView addSubview:nameLabel];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:name forKey:kNameKey];
}

-(void)namePopup:(NSString*)defaultName{
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Name"
                                                                    message:@"What is your name?"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * tf){
        
        tf.delegate = self;
        tf.autocapitalizationType = UITextAutocapitalizationTypeWords;
        tf.text = defaultName;
        [tf addTarget:self
               action:@selector(textFieldDidChange:)
     forControlEvents:UIControlEventEditingChanged];
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Confirm"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * a){
                                                [self setNameLabel:alert.textFields.firstObject.text];
                                                [SoundEffects playNameSelect];
                                            }
                      ]
     ];
    
    [alert.view setBackgroundColor:[ScreenSetup greenColour]];
    
    [self presentViewController:alert
                       animated:YES
                     completion:^{
                         [alert.textFields.firstObject becomeFirstResponder];
                         [SoundEffects playAlert];
                     }];
}

-(void)textFieldDidChange:(UITextField *)textField{
    [SoundEffects playTextType];
}


@end
