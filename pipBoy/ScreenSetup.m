//
//  ScreenSetup.m
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "ScreenSetup.h"
#import "StatDetails.h"
#import "ItemDetails.h"
#import "ScreenData.h"
#import "UIImage+(UIImageColorReplacement).h"
#import "CustomProgressView.h"
#import "ScrollBar.h"

@implementation ScreenSetup

+(UIFont*)monoFont{
    return [UIFont fontWithName:@"Monofonto-Regular" size:12];
}

+(CGRect)screenRect{
    return [[UIScreen mainScreen] bounds];
}

+(CGSize)screenSize{
     return [self screenRect].size;
}

+(UIColor*)torchColour{
    return [[self greenColour] colorWithAlphaComponent:1];
}

+(UIColor*)greenColour{
    return [UIColor colorWithRed:25/255.0 green:1 blue:28/255.0 alpha:.6];
}

+(void)greenUpTextView:(UIView *)textView{
    [(UILabel*)textView setFont:[self monoFont]];
    [(UILabel*)textView setTextColor:[ScreenSetup greenColour]];
    [(UILabel*)textView setBackgroundColor:[UIColor clearColor]];
}

+(void)setupStatsScreens:(Screen *)cnd
                     rad:(Screen *)rad
                     eff:(Screen *)eff
                 special:(Screen *)special
                  skills:(Screen *)skills
                   perks:(Screen *)perks
                 general:(Screen *)general{
    
    NSArray <NSString*>* outlineArray = @[@"CND",@"RAD",@"EFF",@"SPECIAL",@"SKILLS",@"PERKS",@"GENERAL"];
    NSUInteger screenTypes [7] = {CNDType,RADType,EFFType,SpecialType,SkillsType,PerksType,GeneralType};
    
    [self setupUsualScreen:@[special,skills,perks,general]
                allScreens:@[cnd,rad,eff,special,skills,perks,general]
                screnDatas:[StatDetails sharedInstance].datas
               screenTypes:screenTypes
              outlineArray:outlineArray];
    
    [self setupGeneralScreen:general];
    
    //Perks and general table different size
    for (Screen * s in @[general,perks]) {
        
        float tvX = CGRectGetMaxX(s.table.frame) + 10;
        float tvW = 250;
        CGRect rect = CGRectMake(tvX,
                                 [self screenSize].height - 140,
                                 tvW,
                                 98);
        [s.infoTextView setFrame:rect];
    }
    
}


+(void)setupGeneralScreen:(Screen*)screen{
    
    float genOffset = 60;
    
    CGRect tableRect = screen.table.frame;
    tableRect.size.width = 200;
    [screen.table setFrame:tableRect];
    
    CGRect imageRect = screen.infoImageView.frame;
    imageRect.origin.x += genOffset -20; //just trying to center the image in the gap a bit
    imageRect.origin.y += 50;
    imageRect.size.width -= genOffset;
    [screen.infoImageView setFrame:imageRect];
    
    [screen.infoImageView setImage:[[UIImage imageNamed:@"karma"] swapOutWhiteForGreen]];
    
    
    
    UILabel * karmaRating = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 20)];
    [karmaRating setCenter:CGPointMake(CGRectGetMidX(screen.infoImageView.frame),
                                       80)];
    [karmaRating setText:@"Very Good"];
    
    UILabel * bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 40)];
    [bottomLabel setNumberOfLines:0];
    [bottomLabel setText:@"Last, best hope of\nhumanity"];
    [bottomLabel setCenter:CGPointMake(CGRectGetMidX(screen.infoImageView.frame), 250)];
    
    for (UILabel * label in @[karmaRating,bottomLabel]) {
        [label setTextAlignment:NSTextAlignmentCenter];
        [self greenUpTextView:label];
        [screen addSubview:label];
    }
    
    [screen.table reloadData];
    [screen initialSelect];
}


+(void)setupItemsScreen:(Screen *)weapons
                apparel:(Screen *)apparel
                    aid:(Screen *)aid
                   misc:(Screen *)misc
                   ammo:(Screen *)ammo{
    
    NSArray <NSString*>* outlineArray = @[@"WEAPONS",@"APPAREL",@"AID",@"MISC",@"AMMO"];
    NSUInteger screenTypes [5] = {WeaponsType,ApparelType,AidType,MiscType,AmmoType};
    NSArray <Screen*>* screens = @[weapons,apparel,aid,misc,ammo];
    
    //These 3 values are for the items labels' x pos. again - 480 * 320
    const float labelCol1 = 195;
    const float labelCol2 = 300;
    const float labelCol3 = 383;
    
    const float labelRow0 = 170; // apparel dmg res
    const float labelRow1 = 205;
    const float labelRow2 = 240;
    
    const float colWidth = 64;
    const float col1Width = 85; //first col wider in overlays, for some reason. Give me strength.. Get around to fixing this
    const float rowHeight = 20;
    
    for (Screen * screen in screens) {
        
#warning TODO:: This is duplicate in another for loop. Will probably need to rethink this screen type set
        int index = (int)[screens indexOfObject:screen];
        screen.screenType = screenTypes[index];
        
        if (screen.screenType >= WeaponsType && screen.screenType <= AmmoType) {
            UILabel * wg = [[UILabel alloc]initWithFrame:CGRectMake(labelCol2, labelRow1, colWidth, rowHeight)];
            [wg setText:@"WG"];
            [screen addSubview:wg];
            
            UILabel * weight = [[UILabel alloc]initWithFrame:CGRectMake(wg.frame.origin.x,
                                                                        wg.frame.origin.y,
                                                                        colWidth,
                                                                        wg.frame.size.height)];
            [weight setTextAlignment:NSTextAlignmentRight];
            [screen addSubview:weight];
            screen.weightLabel = weight;
            
            UILabel * val = [[UILabel alloc]initWithFrame:CGRectMake(labelCol3,
                                                                     labelRow1,
                                                                     colWidth,
                                                                     rowHeight)];
            [val setText:@"VAL"];
            [screen addSubview:val];
            
            UILabel * value = [[UILabel alloc]initWithFrame:CGRectMake(val.frame.origin.x,
                                                                       val.frame.origin.y,
                                                                       weight.frame.size.width,
                                                                       val.frame.size.height)];
            [value setTextAlignment:NSTextAlignmentRight];
            [screen addSubview:value];
            screen.valueLabel = value;
        }
        
        if (screen.screenType == AidType || screen.screenType == ApparelType) {
            
            UILabel * eff = [[UILabel alloc]initWithFrame:CGRectMake(labelCol1, labelRow2, colWidth, rowHeight)];
            [eff setText:@"EFFECTS"];
            [screen addSubview:eff];
            
            UILabel * effects = [[UILabel alloc]initWithFrame:CGRectMake(eff.frame.origin.x,
                                                                         eff.frame.origin.y,
                                                                         250,
                                                                         eff.frame.size.height)];
            [effects setTextAlignment:NSTextAlignmentRight];
            [screen addSubview:effects];
            screen.effectsLabel = effects;
        }
        
        if (screen.screenType == WeaponsType) {
            UITextView * ammo = [[UITextView alloc]initWithFrame:CGRectMake(labelCol2 - 5, //offset due to uitextview margin
                                                                            labelRow2 - 6,
                                                                            145,
                                                                            40)];
            [ammo setBackgroundColor:[UIColor clearColor]];
            [screen addSubview:ammo];
            screen.ammoTypeTextView = ammo;
            
            UILabel * dmg = [[UILabel alloc]initWithFrame:CGRectMake(labelCol1,
                                                                     labelRow2,
                                                                     colWidth,
                                                                     rowHeight)];
            [dmg setText:@"DMG"];
            [screen addSubview:dmg];
            
            UILabel * damage = [[UILabel alloc]initWithFrame:CGRectMake(dmg.frame.origin.x,
                                                                        dmg.frame.origin.y,
                                                                        col1Width,
                                                                        rowHeight)];
            [damage setTextAlignment:NSTextAlignmentRight];
            [screen addSubview:damage];
            screen.damageLabel = damage;
            
        }
        
        if (screen.screenType == WeaponsType || screen.screenType == ApparelType) {
        
            UILabel * cnd = [[UILabel alloc]initWithFrame:CGRectMake(labelCol1, labelRow1, colWidth, rowHeight)];
            [cnd setText:@"CND"];
            [screen addSubview:cnd];
            
            CustomProgressView * progressView = [[CustomProgressView alloc]initWithFrame:CGRectMake(labelRow1 + colWidth*.38, //magic number to get indent right :/
                                                                                                    labelRow1,
                                                                                                    colWidth,
                                                                                                    rowHeight)];
            [screen addSubview:progressView];
            screen.conditionView = progressView;
        }
        
        if (screen.screenType == ApparelType) {
            
            UILabel *dmgr = [[UILabel alloc]initWithFrame:CGRectMake(labelCol1, labelRow0, col1Width, rowHeight)];
            [dmgr setText:@"DR"];
            [screen addSubview:dmgr];
            
            UILabel * damageRes = [[UILabel alloc]initWithFrame:dmgr.frame];
            [damageRes setTextAlignment:NSTextAlignmentRight];
            [screen addSubview:damageRes];
            screen.damageResLabel = damageRes;
        }
        
    }
    
    [self setupUsualScreen:screens
                allScreens:screens
                screnDatas:[ItemDetails sharedInstance].datas
               screenTypes:screenTypes
              outlineArray:outlineArray];
    
    
}

+(void)setupDataScreen:(Screen *)localMap
              worldMap:(Screen *)worldMap
                quests:(Screen *)quests
                 notes:(Screen *)notes
                 radio:(Screen *)radio{
    
    
}

+(void)setupUsualScreen:(NSArray<Screen*>*)tabledScreens
             allScreens:(NSArray<Screen*>*)allScreens
             screnDatas:(NSArray<ScreenData*>*)datas
            screenTypes:(NSUInteger*)types
           outlineArray:(NSArray<NSString*>*)outlines{
    
    for (Screen * screen in allScreens){
        
        int index = (int)[allScreens indexOfObject:screen];
        screen.screenType = types[index];
        
        UIImageView * bgIv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"background"]];
        [bgIv setFrame:[self screenRect]];
        [bgIv setContentMode:UIViewContentModeScaleToFill];
        [screen addSubview:bgIv];
        [screen sendSubviewToBack:bgIv];
        
        NSString * outlineImageTitle = [outlines objectAtIndex:index];
        
        UIImageView * outline = [[UIImageView alloc]initWithImage:[UIImage imageNamed:outlineImageTitle]];
        [outline setFrame:[self screenRect]];
        [outline setCenter:screen.center];
        [screen addSubview:outline];
        
        [outline setTintColor:[self greenColour]];
        
    }
    
    CGRect tableRect = CGRectMake(25, 0, 160, 240);
    tableRect.origin.y = (320 - tableRect.size.height) / 2; //centralise vertically on 4s
    
    for (Screen * screen in tabledScreens) {
        
        //all magic numbers \/ ^ all of them to fit the 4s, in this case.
                
        PBTableView * table = [[PBTableView alloc]initWithFrame:tableRect];
        [screen addSubview:table];
        table.delegate = screen;
        table.dataSource = screen;
        screen.table = table;
        
        // Table scroll bar

        UIImageView * upScrollArrow = [[UIImageView alloc]initWithFrame:CGRectMake(2.5,
                                                                                   48,
                                                                                   20,
                                                                                   10)];
        [upScrollArrow setTransform:CGAffineTransformRotate(CGAffineTransformIdentity, M_PI)]; //180 deg
        
        UIImageView * downScrollArrow = [[UIImageView alloc]initWithFrame:CGRectMake(upScrollArrow.frame.origin.x + .5,
                                                                                     255,
                                                                                     upScrollArrow.frame.size.width,
                                                                                     upScrollArrow.frame.size.height)];
        
        for (UIImageView * iv in @[upScrollArrow,downScrollArrow]) {
            [screen addSubview:iv];
            [iv setImage:[UIImage imageNamed:@"tableArrow"]];
            [iv setTintColor:[self greenColour]];
            [iv setContentMode:UIViewContentModeScaleAspectFit];
        }
        
        // Scroll bar
        float scrollGap = 5;
        
        ScrollBar * sBar = [[ScrollBar alloc]initWithFrame:CGRectMake(11,
                                                                     CGRectGetMaxY(upScrollArrow.frame) + scrollGap,
                                                                     4,
                                                                     CGRectGetMinY(downScrollArrow.frame) - CGRectGetMaxY(upScrollArrow.frame) - scrollGap*2)];
        [screen addSubview:sBar];
        screen.scrollBar = sBar;
        
        //
        UIImageView * infoImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(table.frame) + 30,
                                                                               50,
                                                                               200,
                                                                               120)];
        [infoImage setContentMode:UIViewContentModeScaleAspectFit];
        [infoImage setTintColor:[self greenColour]];
        [screen addSubview:infoImage];
        screen.infoImageView = infoImage;
        
        //
        
        float tvX = CGRectGetMaxX(table.frame) + 10;
        float tvW = 250;
        
        CGRect rect = CGRectMake(tvX,
                          [self screenSize].height - 125,
                          tvW,
                          83);
        
        
        UITextView * textView = [[UITextView alloc]initWithFrame:rect];
        [screen addSubview:textView];
        [textView setTextAlignment:NSTextAlignmentJustified];
        screen.infoTextView = textView;
        
        
        for (UIView* obj in screen.subviews) {
            if ([obj respondsToSelector:@selector(setText:)]) {
                [self greenUpTextView:obj];
            }
            [screen bringSubviewToFront:obj];
        }
        
    }
    
    
    //Needs to be in its own for loop as it needs to be done last, after the other two loops above 
    for (Screen * screen in tabledScreens) {
        for (ScreenData * data in datas) {
            if (data.type == screen.screenType) {
                screen.data = data;
                [screen.table reloadData];
                [screen initialSelect];
                
                [data setTheScreenType:screen];
            }
        }
    }
}


@end
