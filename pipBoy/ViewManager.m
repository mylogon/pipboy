//
//  ViewManager.m
//  pipBoy
//
//  Created by Luke Sadler on 24/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "ViewManager.h"
#import "ScreenSetup.h"
#import "TheViewController.h"

@interface ViewManager ()
{
    StatusScreens verticalScreen;
    
    StatsScreens statsSection;
    ItemsScreens itemsSection;
    DataScreens dataSection;
    
    SectionsAvailable currentSection;
    
    //Stats
    Screen * cndScreen;
    Screen * radScreen;
    Screen * effScreen;
    
    Screen * specialScreen;
    Screen * skillsScreen;
    Screen * perksScreen;
    Screen * generalScreen;
    
    //Items
    Screen * weaponsScreen;
    Screen * apparelScreen;
    Screen * aidScreen;
    Screen * miscScreen;
    Screen * ammoScreen;
    
    //Data
    Screen * localMapScreen;
    Screen * worldMapScreen;
    Screen * questsScreen;
    Screen * notesScreen;
    Screen * radioScreen;
}
@end

@implementation ViewManager

+(ViewManager *)sharedManager{
    static ViewManager * vm = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        vm = [[ViewManager alloc]init];
    });
    return vm;
}

-(instancetype)init{
    if (self = [super init]) {
        
        statsSection = 0;
        verticalScreen = 0;
        
        cndScreen      = [Screen new];
        radScreen      = [Screen new];
        effScreen      = [Screen new];
        specialScreen  = [Screen new];
        skillsScreen   = [Screen new];
        perksScreen    = [Screen new];
        generalScreen  = [Screen new];
        
        [ScreenSetup setupStatsScreens:cndScreen
                                   rad:radScreen
                                   eff:effScreen
                               special:specialScreen
                                skills:skillsScreen
                                 perks:perksScreen
                               general:generalScreen];
        
        weaponsScreen  = [Screen new];
        apparelScreen  = [Screen new];
        aidScreen      = [Screen new];
        miscScreen     = [Screen new];
        ammoScreen     = [Screen new];
        
        [ScreenSetup setupItemsScreen:weaponsScreen
                              apparel:apparelScreen
                                  aid:aidScreen
                                 misc:miscScreen
                                 ammo:ammoScreen];
        
        localMapScreen = [Screen new];
        worldMapScreen = [Screen new];
        questsScreen   = [Screen new];
        notesScreen    = [Screen new];
        radioScreen    = [Screen new];
        
        [ScreenSetup setupDataScreen:localMapScreen
                            worldMap:worldMapScreen
                              quests:questsScreen
                               notes:notesScreen
                               radio:radioScreen];
    }
    return self;
}

-(Screen *)getInitialScreen{
    return cndScreen;
}

-(void)receivedSwipe:(UISwipeGestureRecognizer *)swipe
         shouldMakeNoise:(void(^)(ScreenChangeType))makeNoise
            assignScreen:(void(^)(Screen*))screen{
    
    ScreenChangeType type = None;
    
    if (swipe.numberOfTouches == ScreenSwipe) {
        if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
            type = [self upSwipe];
        }else if (swipe.direction == UISwipeGestureRecognizerDirectionDown){
            type = [self downSwipe];
        }else if (swipe.direction == UISwipeGestureRecognizerDirectionLeft){
            type = [self leftSwipe];
        }else{
            type = [self rightSwipe];
        }
    }else if(swipe.numberOfTouches == SectionSwipe){
        if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
            type = [self sectionLeft];
        }else if(swipe.direction == UISwipeGestureRecognizerDirectionRight){
            type = [self sectionRight];
        }
    }
    
    makeNoise(type);
    screen(type == None ? nil : [self getScreen]);
}

//a kind of state machine
-(Screen *)getScreen{

    switch (currentSection) {
        case Stats:
        {
            switch (statsSection) {
                case StatusHoriz:
                {
                    switch (verticalScreen) {
                        case CndVert:
                            return cndScreen;
                        case RadVert:
                            return radScreen;
                        case EffVert:
                            return effScreen;
                        default:
                            break;
                    }
                }
                    break;
                case SpecialHoriz:
                    return specialScreen;
                case SkillsHoriz:
                    return skillsScreen;
                case PerksHoriz:
                    return perksScreen;
                case GeneralHoriz:
                    return generalScreen;
                default:
                    break;
            }
        }
            break;
            case Items:
        {
            switch (itemsSection) {
                case Aid:
                    return aidScreen;
                case Ammo:
                    return ammoScreen;
                case Apparel:
                    return apparelScreen;
                case Misc:
                    return miscScreen;
                case Weapons:
                    return weaponsScreen;
                default:
                    break;
            }
        }
        case Data:
        {
            switch (dataSection) {
                case LocalMap:
                    return localMapScreen;
                case Notes:
                    return notesScreen;
                case Quests:
                    return questsScreen;
                case Radio:
                    return radioScreen;
                case WorldMap:
                    return worldMapScreen;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return nil;
}

#warning get rid of this -1 once Data is ready to develop
-(ScreenChangeType)sectionRight{
    if (currentSection +1 <= Data-1) {
        currentSection ++;
        return SectionChange;
    }
    return None;
}

-(ScreenChangeType)sectionLeft{
    if (currentSection -1 >= Stats) {
        currentSection --;
        return SectionChange;
    }
    return None;
}

-(ScreenChangeType)rightSwipe{
    
    if (currentSection == Stats) {
        if (statsSection +1 <= GeneralHoriz) {
            
            statsSection++;
            return HorizonalChange;
        }
    }else if (currentSection == Items){
        if (itemsSection +1 <= Ammo) {
            itemsSection ++;
            return HorizonalChange;
        }
    }else if (currentSection == Data){
        if (dataSection +1 <= Radio) {
            dataSection++;
            return HorizonalChange;
        }
    }
    
    return None;
}

-(ScreenChangeType)leftSwipe{
    if (currentSection == Stats) {
        if (statsSection -1 >= StatusHoriz) {
            
            statsSection--;
            return HorizonalChange;
        }
    }else if (currentSection == Items){
        if (itemsSection -1 >= Weapons) {
            itemsSection --;
            return HorizonalChange;
        }
    }else if (currentSection == Data){
        if (dataSection -1 >= Radio) {
            dataSection--;
            return HorizonalChange;
        }
    }
    return None;
}

-(ScreenChangeType)upSwipe{
    if (currentSection == Stats && statsSection == StatusHoriz && verticalScreen -1 >= CndVert) {
        verticalScreen --;
        return VerticalChange;
    }
    return None;
}

-(ScreenChangeType)downSwipe{
    if (currentSection == Stats && statsSection == StatusHoriz && verticalScreen +1 <= EffVert) {
        verticalScreen ++;
        return VerticalChange;
    }
    return None;
}

@end
