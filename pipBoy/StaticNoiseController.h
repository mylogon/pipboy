//
//  StaticNoiseController.h
//  pipBoy
//
//  Created by Luke Sadler on 26/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BumpMotion.h"

@class TheViewController;
@interface StaticNoiseController : NSObject <BumpMotionDelegate>

-(instancetype)initWithViewController:(TheViewController*)vc
                        andTouchLayer:(UIView*)touchView;

/**Pass Yes if it was a horizontal transition*/
-(NSTimeInterval)makeSingleStatic:(BOOL)horizonal;

/**Call when torch gets turned on*/
-(void)pauseStaticLines;
/**Call when torch gets turned off*/
-(void)resumeStaticLines;

@end
