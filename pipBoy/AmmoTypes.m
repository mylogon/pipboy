//
//  AmmoTypes.m
//  pipBoy
//
//  Created by Luke Sadler on 20/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import "AmmoTypes.h"

@implementation AmmoTypes

+(NSString*)getTitle:(TheAmmoType)type{
    
    switch (type) {
        case kNone:
            return @"--";
        case k10mm:
            return @"10mm (30/1604)";
        case kMFCell:
            return @"MF Cell (12/748)";
        case kAlienPowerCell:
            return @"Alien Power Cell (10/128)";
        case k44:
            return @".44 (6/284)";
        case k556:
            return @"5.56mm (24/1791)";
        case kECell:
            return @"Energy Cell (30/3091)";
        case kDart:
            return @"Dart (1/1171)";
        case kMissile:
            return @"Missile (1/165)";
        case kMiniNuke:
            return @"Mini Nuke (1/10)";
        case kMezPowerCell:
            return @"Mez. Power Cell (5/42)";
        case k5mm:
            return @"5mm (240/2348)";
        case k32:
            return @".32 (5/611)";
        case kSpikes:
            return @"Spikes (8/278)";
        case kShell:
            return @"Shell (12/528)";
        case kBB:
            return @"BB (30/30)";
        case kFlamerFuel:
            return @"Flamer Fuel (200/2500)";
        case kElectron:
            return @"Electron Charge Pack (180/500)";
        case kRockItLauncher:
            return @"Rock-It Launcher ammo (160/180)";
        case k308:
            return @".308 Caliber Round (186/200)";
        default:
            break;
    }
    
}

+(NSString *)getAmmoTitle:(TheAmmoType)type{
    return [[[self getTitle:type] componentsSeparatedByString:@"/"].firstObject stringByAppendingString:@")"];
}

@end
