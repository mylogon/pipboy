//
//  TheViewController.h
//  pipBoy
//
//  Created by Luke Sadler on 01/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ScreenSwipe = 1,
    SectionSwipe = 2,
    TorchTouch = 2
} SwipeFingerCount;

@interface TheViewController : UIViewController

@end
