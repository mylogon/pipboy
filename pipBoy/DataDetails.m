//
//  DataDetails.m
//  pipBoy
//
//  Created by Luke Sadler on 05/01/2017.
//  Copyright © 2017 Luke Sadler. All rights reserved.
//

#import "DataDetails.h"

@interface DataDetails ()
{
    ScreenData * localMapData;
    ScreenData * notesData;
    ScreenData * questData;
    ScreenData * radioData;
    ScreenData * worldMapData;
}
@end
@implementation DataDetails

@end
