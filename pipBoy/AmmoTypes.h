//
//  AmmoTypes.h
//  pipBoy
//
//  Created by Luke Sadler on 20/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    kNone,
    k10mm,
    kMFCell,
    kAlienPowerCell,
    k44,
    k556,
    kECell,
    kDart,
    kMiniNuke,
    kMezPowerCell,
    k5mm,
    kMissile,
    k32,
    k308,
    kSpikes,
    kShell,
    kBB,
    kFlamerFuel,
    kElectron,
    kRockItLauncher
} TheAmmoType;

@interface AmmoTypes : NSObject

+(NSString*)getTitle:(TheAmmoType)type;
+(NSString*)getAmmoTitle:(TheAmmoType)type;
@end
