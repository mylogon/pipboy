//
//  ScreenEnums.h
//  pipBoy
//
//  Created by Luke Sadler on 02/12/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#ifndef ScreenEnums_h
#define ScreenEnums_h


typedef enum : NSInteger {
    None,
    HorizonalChange,
    VerticalChange,
    SectionChange
} ScreenChangeType;

typedef enum : NSInteger {
    Stats,
    Items,
    Data,
} SectionsAvailable;

//Stats
typedef enum : NSInteger {
    StatusHoriz,
    SpecialHoriz,
    SkillsHoriz,
    PerksHoriz,
    GeneralHoriz
} StatsScreens;

typedef enum : NSInteger {
    CndVert,
    RadVert,
    EffVert,
} StatusScreens;

//Items
typedef enum : NSInteger {
    Weapons,
    Apparel,
    Aid,
    Misc,
    Ammo,
} ItemsScreens;

//Data
typedef enum : NSInteger {
    LocalMap,
    WorldMap,
    Quests,
    Notes,
    Radio
} DataScreens;

typedef enum : NSUInteger {
    CNDType,
    RADType,
    EFFType,
    SpecialType,
    SkillsType,
    PerksType,
    GeneralType,
    
    WeaponsType,
    ApparelType,
    AidType,
    MiscType,
    AmmoType,
    
    LocalMapType,
    WorldMapType,
    QuestsType,
    NotesType,
    RadioType
} ScreenType;

#endif /* ScreenEnums_h */
