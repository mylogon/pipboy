//
//  ScreenSetup.h
//  pipBoy
//
//  Created by Luke Sadler on 25/11/2016.
//  Copyright © 2016 Luke Sadler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Screen.h"

@interface ScreenSetup : NSObject

+(UIColor*)greenColour;
+(UIColor*)torchColour;
+(UIFont*)monoFont;

/**Sets the font, colour and background*/
+(void)greenUpTextView:(UIView*)textView;

+(void)setupStatsScreens:(Screen *)cnd
                     rad:(Screen *)rad
                     eff:(Screen *)eff
                 special:(Screen *)special
                  skills:(Screen *)skills
                   perks:(Screen *)perks
                 general:(Screen *)general;

+(void)setupItemsScreen:(Screen *)weapons
                apparel:(Screen *)apparel
                    aid:(Screen *)aid
                   misc:(Screen *)misc
                   ammo:(Screen *)ammo;

+(void)setupDataScreen:(Screen* )localMap
              worldMap:(Screen* )worldMap
                quests:(Screen* )quests
                 notes:(Screen* )notes
                 radio:(Screen* )radio;

@end
