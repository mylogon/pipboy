# README #

This is my interpretation of the Fallout 3 Pipboy. This app is specifically designed for the iPhone/iPod 320x480 display (iPhone 4s and less) as it fits perfectly into the moulds of the pipboy alarm clock thingy that most people have.

Feel free to contribute to branch & pull request if you want to contribute some work towards this.

Code should be fairly well commented up, but if anything is unclear then contact me via ... *insert method here?*


If you don't want to/ can't compile it to put on device, then grab the latest version from [**here**](https://pipboyappbuilds.000webhostapp.com/builds/) to install onto your (jailbroken) device

# How to operate the app #

After inputting your name, you can navigate the app with swipe gestures.

* One finger swipes changes the page you're on and scrolls through tables
* Top-of-table-tap scrolls to top of table
* Two finger swipes changes the section you're on
* Double, two-finger tap turns the app torch on and off
* A dumb/ fun feature is that you can hit your device and creates scan lines/ interference
* App settings are in iOS settings section. Here you can set the 'bump tolerance' (lower = easier to trigger above feature) and background static volume

# TODO #
**Pretty much whole 'Data' section**

^ with that I've got some good world/local map ideas. All holotapes and significant radio stations will also be include.

* Item condition UI element
* Fix/ rethink apparel overlay
* Create custom scroll bar for tables